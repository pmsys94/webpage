<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 11;
include '../php/std-head.inc';
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <style type="text/css">
        	div#lcont{
        		width: 100%;
                margin-left: unset;
        		margin-right: unset;
        	}
        	#lcont a{
        		display: block;
        	}
            #lcont > div{
                display: block;
                margin-left: unset;
                margin-right: unset;
            }
        	@media (min-width: 601px){
        		div#lcont{
        			width: 80%;
        			margin-left: 5%;
        			margin-right: 5%;
        		}
                #lcont > div{
                    display: unset;
                    margin-left: 16px;
                    margin-right: 16px;
                }
        	}
        </style>
    </head>
    <body>
<?php include '../php/std-menu-head.inc'; echo "\n";?>
		<div id="Inhalt">
			<h1 class="w3-section w3-margin-left">Software</h1>
			<p class="w3-padding-large">
                <?php ParaAreaStd($AreaStore["SoftwareIntrests"]);?>
			</p>
			<div id="lcont">
<?php
$w3_colors = array("w3-teal", "w3-lime", "w3-purple", "w3-khaki", "w3-gray");
$i = 0;

include 'subitems.inc';
while ($si = mysqli_fetch_array($siR)) {
	echo "					<div class=\"w3-quarter\">\n";
	echo "						<a href=\"" . urlencode($si["URL"]) . $link_lang . "\" class=\"w3-text-white " . $w3_colors[$i] . " w3-xlarge\">" . htmlfix($si["EntryText"]) . "</a>\n";
	echo "					</div>\n";
	if($i < count($w3_colors)){
		$i++;
	} else {
		$i = 0;
	}
}
?>
			</div>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>