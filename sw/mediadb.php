<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 14;
include '../php/std-head.inc';
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <style type="text/css">
        	.grid-cell{
                margin: 5px;
                float: left;
                width: 99%;
                background-color: white;
            }
            .grid-container{
                width: 100%;
                max-width: 1600px;
                margin: auto;
            }
            .flex-container{
                display: flex;
                flex-wrap: wrap;
            }
        	img{
        		width: 100%;
        	}
        	.mdb-bgcolor{
        		background-color: #75D08C;
        		margin: 0;
        	}
        	#phoneScreen{
        		max-width: 49%;
        		float: left;
        	}
        	#mdbPict1{
        		margin-bottom: 10px;
        	}
        	.pictLeftSpace{
        		float: left;
        	}
        	@media (min-width:  600px){
        		#pictWebDesktop{
        			max-width: 600px;
        		}
        		#divPictWebDesktop{
        			max-width: 49.9%;
        		}
        		.NextExcelMargin{
        			margin-right: 10px;
        		}
        		#phoneScreen{
        			max-width: 20%;
        		}
        		#mdbPict1{
        		max-width: 49.9%;
        		margin-right: 10px;
        		margin-bottom: unset;
        		}
        		#mdbPict2{
        			max-width: 40%;
        		}
        		.grid-cell{
        			max-width: 31%;
        		}
        	}
        	@media (min-width:  1200px){
        		#phoneScreen{
        			max-width: 13%;
        		}
        		#mdbPict1{
        			max-width: 30%;
        		}
        		#mdbPict2{
        			max-width: 25%;
        		}
        		.pictLeftSpace{
        			float: unset;
        		}
        	}
        </style>
    </head>
    <body>
<?php
include '../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px;">
			<h1>MediaDB</h1>
			<h2 style="font-style: italic;"><?php echo $AreaStore["subtitle"]; ?></h2>
			<div class="w3-mobile w3-right" id="divPictWebDesktop"><img src="/img/mediadb/screen-web-desktop.png" style="margin-top: 5px;" id="pictWebDesktop"></div>
			<h3><?php echo $AreaStore["headerAbout"]; ?></h3>
			<p><?php ParaAreaStd($AreaStore["AboutMediaDB"]); ?></p>
			<div class="w3-mobile w3-left NextExcelMargin"><img src="/img/mediadb/excel-screen.PNG" style="max-width: 300px;"></div>
			<h3><?php echo $AreaStore["excelTables"]; ?></h3>
			<p><?php ParaAreaStd($AreaStore["aboutUseExcel"]); ?></p>
			<div style="display: block;">
				<img src="/img/mediadb/phone-screen.png" id="phoneScreen">
				<h3><?php echo $AreaStore["headePhoneSite"];?></h3>
				<p><?php ParaAreaStd($AreaStore["phoneDisplay"]); ?></p>
			</div>
			<div style="float: left;">
				<h3>MediaDB-Tool</h3>
				<h4 style="font-style: italic;"><?php echo $AreaStore["supportFromPC"]; ?></h4>
				<p><?php ParaAreaStd($AreaStore["mdb-tool1"]); ?></p>
				<img id="mdbPict1" src="/img/mediadb/mdbtool-main.PNG" style="float: right;"><img id="mdbPict2" src="/img/mediadb/mdbtool-fn2track.PNG" style="float: right;">
				<h4 style="font-style: italic;" class="pictLeftSpace"><?php echo $AreaStore["fn2track"]; ?></h4>
				<p class="pictLeftSpace"><?php ParaAreaStd($AreaStore["mdb-tool2"]); ?></p>
			</div>
			<div style="float: left;">
				<h3><?php echo $AreaStore["pythonPort"]; ?></h3>
				<p><?php ParaAreaStd($AreaStore["aboutPythonPort"]); ?></p>
			</div>
			<div style="float: left; margin-bottom: 5px; margin-right: 5px;" class="w3-card-2 w3-padding">
                <h3>Features</h3>
				<div class="w3-container grid-container">
					<div class="flex-container">
						<div class="w3-card grid-cell">
							<h5 class="mdb-bgcolor"><?php echo $AreaStore["F_Listings"]; ?></h5>
							<img src="/img/mediadb/screen-DDISK.png" class="w3-image">
							<p class="w3-small"><?php ParaAreaStd($AreaStore["listings"]); ?></p>
						</div>
						<div class="w3-card grid-cell">
							<h5 class="mdb-bgcolor"><?php echo $AreaStore["F_Modal"]; ?></h5>
							<img src="/img/mediadb/screen-modal.png" class="w3-image">
							<p><?php ParaAreaStd($AreaStore["modalTables"]); ?></p>
						</div>
						<div class="w3-card grid-cell">
							<h5 class="mdb-bgcolor"><?php echo $AreaStore["F_search"]; ?></h5>
							<img src="/img/mediadb/screen-search.png" class="w3-image">
							<p class="w3-small"><?php ParaAreaStd($AreaStore["searching"]); ?></p>
						</div>
					</div>
					<div class="flex-container">
						<div class="w3-card grid-cell">
							<h5 class="mdb-bgcolor"><?php echo $AreaStore["F_Stotviewer"]; ?></h5>
							<img src="/img/mediadb/screen-slotviewer.png" class="w3-image">
							<p class="w3-small"><?php ParaAreaStd($AreaStore["slotviewer"]); ?></p>
						</div>
						<div class="w3-card grid-cell">
							<h5 class="mdb-bgcolor"><?php echo $AreaStore["F_Detailview"]; ?></h5>
							<img src="/img/mediadb/screen-detailview.png" class="w3-image">
							<p><?php ParaAreaStd($AreaStore["detailview"]); ?></p>
						</div>
						<div class="w3-card grid-cell">
							<h5 class="mdb-bgcolor"><?php echo $AreaStore["F_Tracks"]; ?></h5>
							<img src="/img/mediadb/screen-detailview.png" class="w3-image">
							<p><?php ParaAreaStd($AreaStore["tracklist"]); ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>