<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 12;
include '../php/std-head.inc';
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <style type="text/css">
            #img-wb6{
                float: right;
                width: 100%;
                height: auto;
                max-width: 800px;
                padding: 5px;
            }
            .grid-cell{
                margin: 5px;
                float: left;
                width: 99%;
                max-width: 31.89%;
                min-width: 300px;
                background-color: white;
            }
            .grid-container{
                width: 100%;
                max-width: 1600px;
                margin: auto;
            }
            .wb-MenColor{
                background-color: hsl(210, 100%, 60%);
                color: black;
                margin: 0;
            }
            ul.wb-Propety li{
                list-style: none;
                float: left;
                display: block;
                background-color: hsl(210, 100%, 60%);
                color: black;
                margin-left: 5px;
                margin-right: 5px;
            }
            .flex-container{
                display: flex;
                flex-wrap: wrap;
            }
            #img-wbTrans{
                width: 100%;
                height: auto;
                max-width: 300px;
                float: right;
            }
            @media (min-width: 801px){
                .wb-SettingImg{
                    max-width: 70%;
                }
            }
            @media (max-width: 801px){
                #img-wbTrans{
                    width: 30%;
                }
            }
            @media (max-width: 601px){
                #img-wbTrans{
                    display: block;
                    float: unset;
                    width: 60%;
                }
            }
        </style>
    </head>
    <body>
<?php
include '../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px;">
            <img src="/img/wb/Globe.ico" style="width: 48px; height: auto; float: left; margin-top: 5px;">
            <h1 style="display: block;">Webbird Webbrowser</h1>
            <h2><?php echo $AreaStore["PageSubHeader"]; ?></h2>
            <p>
<?php ParaAreaStd($AreaStore["AboutBrowsers"]); ?>
            </p>
            <!-- Area about application idea -->
            <div style="display: block;">
                <h3><?php echo $AreaStore["AboutWB_Header"]; ?></h3>
                <img src="/img/WB-Screenshot.png" id="img-wb6">
                <p>
<?php
$para = $AreaStore["AboutWB"];
$entstr = htmlfix($para);
$nlstr = nl2br($entstr);
$wwstr = wordwrap($nlstr, 240, "\n", false);
echo str_replace(array("*", "#"), array("<b>", "</b>"), $wwstr) . "\n";
?>
                </p>
            </div>
            <div style="display: block; margin-bottom: 5px; margin-right: 5px;" class="w3-card-2 w3-padding">
                <h3>Features</h3>
                <div class="w3-container grid-container">
                    <div class="flex-container">
                        <!-- PE -->
                        <div class="w3-card grid-cell">
                            <h5 class="wb-MenColor">Portable Environment</h5>
                            <img src="/img/wb/wb-portable.PNG" class="w3-image w3-opacity" style="max-width: 387px; width: 100%;border: 2px solid !important;">
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["PE_Feature"]); ?>
                            </p>
                        </div>
                        <!-- Auto Refresh -->
                        <div class="w3-card grid-cell">
                            <h5 class="wb-MenColor">Auto Refresh</h5>
                            <img src="/img/wb/wb-autorefresh.PNG" class="w3-image w3-opacity" style="border: 2px solid !important;">
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["Feature_Autorefresh"]); ?>
                            </p>
                        </div>
                        <!-- Move Home -->
                        <div class="w3-card grid-cell">
                            <h5 class="wb-MenColor">Moved Home</h5>
                            <img src="/img/wb/wb-move-home.PNG" class="w3-image wb-SettingImg">
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["MVH"]); ?>
                            </p>
                        </div>
                    </div>
                    <div class="flex-container">
                        <!-- Downloader -->
                        <div class="w3-card grid-cell">
                            <h5 class="wb-MenColor"><?php echo $AreaStore["FeaturesDownloaderH5"];  ?></h5>
                            <img src="/img/wb/wb-downloaderUI.PNG" class="w3-image wb-SettingImg">
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["DownloaderFeature"]); ?>
                            </p>
                        </div>
                        <!-- Language Packs -->
                        <div class="w3-card grid-cell">
                            <h5 class="wb-MenColor">MUI</h5>
                            <img src="/img/wb/wb-LangUI.PNG" class="w3-image wb-SettingImg">
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["MUI"]); ?>
                            </p>
                        </div>
                        <!-- WB Touch -->
                        <div class="w3-card grid-cell">
                            <h5 class="wb-MenColor"><?php echo $AreaStore["FeaturesWBT_Header"]; ?></h5>
                            <img src="/img/wb/wb7.PNG" class="w3-image wb-SettingImg">
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["WBT"]); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3-card-4 w3-padding" style="display: block; margin-bottom: 5px; margin-right: 5px;">
                <h3><?php echo $AreaStore["VersionsCard"]; ?></h3>
                <!-- Webbird 7 -->
                <div class="w3-card w3-padding">
                    <h5>Version 7 [WBT] (~ 2015-2016)</h5>
                    <div style="float: left; margin-bottom: 10px; width: 100%;">
                        <ul class="wb-Propety">
                            <li>.NET-Framework: 4.0</li>
                            <li>Windows: XP-10</li>
                            <li>Engine: Gecko 29.0.9-33.0.3</li>
                            <li><?php echo $AreaStore["InDevVersion"]; ?></li>
                        </ul>
                    </div>
                    <br>
                    <p>
<?php ParaAreaStd($AreaStore["WBT_Version"]); ?>
                    </p>
                    <p>
<?php ParaAreaStd($AreaStore["Version7"]); ?>
                    </p>
                </div>
                <hr>
                <!-- Webbird 6 -->
                <div class="w3-card w3-padding" style="display: flow-root;">
                    <h5>Version 6 (~ 2013-2014)</h5>
                    <div style="float: left; margin-bottom: 10px; width: 100%;">
                        <ul class="wb-Propety">
                            <li>.NET-Framework: 4.0</li>
                            <li>Windows: XP-10</li>
                            <li>Engine: Gecko 22.0.0-29.0.0</li>
                            <li><?php echo $AreaStore["LastStable"]; ?></li>
                        </ul>
                    </div>
                    <br>
                    <img src="/img/wb/mb2wb-transformator.PNG" id="img-wbTrans">
                    <p>
<?php ParaAreaStd($AreaStore["Version6"]); ?>
                    </p>
                </div>
                <hr>
                <!-- Webird 5 -->
                <div class="w3-card w3-padding" style="display: flow-root;">
                    <h5>Version 5 (~ 2012-2013)</h5>
                    <div style="float: left; margin-bottom: 10px; width: 100%;">
                        <ul class="wb-Propety">
                            <li>.NET-Framework: 2.0</li>
                            <li>Windows: 98-8.1</li>
                            <li>Engine: IE</li>
                        </ul>
                    </div>
                    <br>
                    <img src="/img/wb/mb-ver5021-mainUI.PNG" style="width: 100%; height: auto; max-width: 450px; float: right; border: 2px solid !important; margin-top: 5px;">
                    <p>
<?php ParaAreaStd($AreaStore["Version5"]); ?>
                    </p>
                </div>
                <hr>
                <!-- Webbird 4 -->
                <div class="w3-card w3-padding">
                    <h5>Version 4 (~ 2011-2012)</h5>
                    <ul class="wb-Propety">
                        <li>.NET-Framework: 2.0</li>
                        <li>Windows: 98-7</li>
                        <li>Engine: IE</li>
                    </ul>
                    <br>
                    <p>
<?php ParaAreaStd($AreaStore["Version4"]); ?>
                    </p>
                </div>
                <hr>
                <!-- Webbird 3 -->
                <div class="w3-card w3-padding">
                    <h5>Version 3 (~ 2011-2012)</h5>
                    <ul class="wb-Propety">
                        <li>.NET-Framework: 2.0</li>
                        <li>Windows: 98-7</li>
                        <li>Engine: IE</li>
                    </ul>
                    <br>
                    <p>
<?php ParaAreaStd($AreaStore["Version3"]); ?>
                    </p>
                </div>
                <hr>
                <!-- Webbird 2 -->
                <div class="w3-card w3-padding">
                    <h5>Version 2 (~ 2011)</h5>
                    <ul class="wb-Propety">
                        <li>.NET-Framework: 2.0</li>
                        <li>Windows: 98-7</li>
                        <li>Engine: IE</li>
                    </ul>
                    <br>
                    <p>
<?php ParaAreaStd($AreaStore["Version2"]); ?>
                    </p>
                </div>
                <hr>
                <!-- Webbid 1 -->
                <div class="w3-card w3-padding">
                    <h5>Version 1 (~ 2010)</h5>
                    <ul class="wb-Propety">
                        <li>.NET-Framework: 2.0</li>
                        <li>Windows: 98-XP</li>
                        <li>Engine: IE</li>
                    </ul>
                    <br>
                    <p>
<?php ParaAreaStd($AreaStore["Version1"]); ?>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>