<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 15;
include '../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, 0); // Also include dummy page areas
function ParaAreaRepl($replstr, $img_id){
	$lmark_idx = strpos($replstr, "*");
	$rmark_idx = strpos($replstr, "#");
	$llength = ($rmark_idx-1)-$lmark_idx;
	$ltxt = substr($replstr, $lmark_idx+1, $llength); // start-pos: after *, length: (end-1)-start
	$cutstr = substr($replstr, 0, $lmark_idx) . "~" . substr($replstr, $rmark_idx+1); // lmark_idx == length up to * | add a ~ as new repl mark | concat rest of replstr after # (no length == auto length)
	$entity = htmlfix($cutstr);
	$links_str = "<a href=\"#" . $img_id . "\" class=\"link2anchor\">" . $ltxt . "</a>";
	$links_str .= "<a href=\"javascript:void\" class=\"link2modal\" onclick=\"imgClick(document.getElementById('" . $img_id . "'))\">" . $ltxt . "</a>";
	$lstr = str_replace("~", $links_str, $entity); // put our doubled-links to our str
	$brStr = nl2br($lstr);
	echo wordwrap($brStr, 240, "\n", false);
}
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <script type="text/javascript" src="/js/pict-modal/pictmodal.js"></script>
        <link rel="stylesheet" type="text/css" href="/js/pict-modal/pictmodal-style.css">
        <style type="text/css">
        	#invsvc-arch-img{
        		width: 100%;
        		float: right;
        	}
        	.link2modal{
        		display: none;
        	}
        	.small-float{
        		float: right;
        	}
        	@media (min-width: 500px){
        		.pictDiv{
        			margin-right: 5px;
        			margin-left: 5px;
        			float: right;
        		}
        		.small-float{
        			float: unset;
        		}
        	}
        	@media (min-width:  800px){
        		#invsvc-arch-img{
        			width: 30%;
        		}
        		.link2modal{
        			display: unset;
        		}
        		.link2anchor{
        			display: none;
        		}
        	}
			@media (min-width: 1200px){
				#invsvc-arch-img{
					width: 20%;
				}
			}
        </style>
    </head>
    <body onload="pictmodal_init()">
<?php
include '../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
			<h1><?php echo $AreaStore["page-title"]; ?></h1>
			<h3><?php echo $AreaStore["invt-desc"]; ?></h3>
			<p>
<?php ParaAreaStd($AreaStore["invt-idea"]); ?>
			</p>
			<h4><?php echo $AreaStore["concept"]; ?></h4>
			<p>
<?php ParaAreaStd($AreaStore["invt-app-concept"]); ?>
			</p>
			<h4>Invoice Service</h4>
			<div id="invsvc-arch-img" class="pictDiv" style="min-width: 150px; max-width: 451px;">
				<img src="/img/invsvc/invsvc-arch.jpg" class="w3-image">
				<p class="w3-small"><?php echo $AreaStore["capt1"]; ?></p>
			</div>
			<p>
<?php ParaAreaStd($AreaStore["invsvc-desc"]); ?>
			</p>
			<h4>Invoice Tool</h4>
			<div class="pictDiv" style="width: 100%; max-width: 419px;">
				<img id="invt-create-form" src="/img/invsvc/invt-create-form.JPG" class="w3-image" onclick="imgClick(this)"
					 <?php echo "alt=\"" . $AreaStore["capt2"] . "\""; ?>><!-- PHP writes alt attr to img tag here and next img below -->
				<p class="w3-small"><?php echo $AreaStore["capt2"] . ". " . $AreaStore["Click2Zoom"]; ?></p>		
			</div>
			<div class="pictDiv" style="width: 100%; max-width: 280px; clear: both;">
				<img id="invt-pdf-viewer" src="/img/invsvc/invt-pdf-viewer.JPG" class="w3-image" onclick="imgClick(this)"
					 <?php echo "alt=\"" . $AreaStore["capt3"] . "\""; ?>>
				<p class="w3-small"><?php echo $AreaStore["capt3"] . ". " . $AreaStore["Click2Zoom"]; ?></p>
			</div>
			<p class="small-float">
<?php ParaAreaRepl($AreaStore["invt-makeInv"], "invt-create-form"); ?>		
			</p>
			<p class="small-float">
<?php ParaAreaRepl($AreaStore["invt-makeInv2"], "invt-pdf-viewer"); ?>
			</p>
			<h4><?php echo $AreaStore["end-of-development"]; ?></h4>
			<p>
<?php ParaAreaStd($AreaStore["invsvc-eod"]); ?>
			</p>
		</div>
		<!-- The Modal -->
		<div id="pictmodal" class="modal">
  			<span class="close" onclick="spanClick()">&times;</span>
  			<img class="modal-content" id="modimg">
  			<div id="caption"></div>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>