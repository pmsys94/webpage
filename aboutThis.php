<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 13;
include 'php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, 0); // Also include dummy page areas
?>
		<link rel="icon" href="pm_fav.ico">
		<title><?php echo $page["HTML_Title"]; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/styles/w3.css">
		<link rel="stylesheet" href="./styles/menu_and_main.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="/styles/aThis.css">
	</head>
	<body>
		<?php include 'php/std-menu-head.inc'; echo "\n";?>
		<div id="Inhalt" class="w3-container">
			<h3><?php echo $AreaStore["PageHeader"]; ?></h3>
			<div class="w3-card-4">
				<h4 class="w3-teal"><?php echo $AreaStore["AboutConten_H4"]; ?></h4>
				<p>
<?php 
$txt = $AreaStore["AboutPage"];
$txt_main = pullArea($DBcon, "h4Side", $lang, 1);
$txt = str_replace("*", $txt_main, $txt);
$entity = htmlfix($txt);
$brStr = nl2br($entity);
echo wordwrap($brStr, 240, "\n", false) . "\n"; 
?>
				</p>
			</div>
			<div class="w3-card-4">
				<h4 class="w3-teal"><?php echo $AreaStore["H4_WhatItNotIs"]; ?></h4>
				<p>
<?php ParaAreaStd($AreaStore["WhatItNotIs"]); ?>
				</p>
			</div>
			<div class="w3-container w3-card-4" style="padding: 0;">
				<h4 class="w3-teal" style="margin: 0;"><?php echo $AreaStore["AboutTech_h"]; ?></h4>
				<p style="float: left;"><?php echo $AreaStore["WordPrefChoice"]; ?></p>
				<input type="radio" name="dispDiv" id="dispHTMLbox" checked="">
				<input type="radio" name="dispDiv" id="dispPHPbox">
				<input type="radio" name="dispDiv" id="dispDBbox">
				<label for="dispHTMLbox"><h5><?php echo $AreaStore["HTML_and_CSS"]; ?></h5></label>
				<label for="dispPHPbox"><h5><?php echo $AreaStore["ProgrammingWithPHP"]; ?></h5></label>
				<label for="dispDBbox"><h5><?php echo $AreaStore["DB_Transl"]; ?></h5></label>
				<!-- HTML & CSS Tech part -->
				<div class="techCard" id="HTMLbox">
					<p>
<?php 
$txt = $AreaStore["paraHTML_CSS"];
$txt = htmlfix($txt);
$txt = str_replace(array("*", "#"), array("<a class=\"TutoLink\" href=\"https://www.w3schools.com/w3css/default.asp\">", "</a>"), $txt);
$txt = str_replace(array("+", "~"), array("<a class=\"TutoLink\" href=\"https://www.html-seminar.de/toggle-menue-ohne-javascript.htm\">", "</a>"), $txt);
$brStr = nl2br($txt);
echo wordwrap($brStr, 240, "\n", false) . "\n";
?>
					</p>
				</div>
				<!-- PHP Programmming tech part -->
				<div class="techCard" id="PHPbox">
					<p>
<?php ParaAreaStd($AreaStore["ProgPHP_p"]) ?>
					</p>
				</div>
				<!-- DB tech part -->
				<div class="techCard" id="DBbox">
					<p>
<?php ParaAreaStd($AreaStore["ParaDB"]) ?>
					</p>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
mysqli_close($DBcon);
?>