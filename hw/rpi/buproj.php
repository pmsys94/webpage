<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 7;
include '../../php/std-head.inc';
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <style type="text/css">
            code{
                font-size: 12px;
                line-height: 0;
            }
            #remsh code{
                font-size: 7px;
            }
            #remsh{
                width: 100%;
                display: block;
            }
            #swapp{
                height: auto;
                width: 100%;
                max-width: 200px;
                margin: auto;
            }
            @media (min-width: 600px){
                #swapp{
                    float: left;
                    margin: unset;
                }
                #remsh{
                    width: unset;
                    display: unset;
                    float: left;
                }
                #remsh code{
                font-size: 12px;
                }
            }
            @media (min-width: 1600px){
                #CircuitImg{
                    float: right;
                }
            }
        </style>
    </head>
    <body>
<?php
include '../../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px;">
            <h3><?php echo $AreaStore["buprojHead"]; ?></h3>
            <!-- Intro Para -->
            <div>
                <h4><?php echo $AreaStore["BlockIdea"]; ?></h4>
                <h5><?php echo $AreaStore["IdeaMissingSolution"]; ?></h5>
                <p class="w3-medium">
<?php ParaAreaStd($AreaStore["IdeaSolutionDescr"]); ?>
                </p>
                <h5><?php echo $AreaStore["IdeaBuildRpiStorage"]; ?></h5>
                <p class="w3-medium">
<?php ParaAreaStd($AreaStore["IdeaRpiSolution"]); ?>
                </p>
            </div>
            <!-- Electronic Circuit & Switch Script -->
            <div style="width: 100%; float: left;">
                <!-- Circuit -->
                <div class="w3-half w3-padding">
                    <h4><?php echo $AreaStore["HeadingCircuit"]; ?></h4>
                    <img src="/img/buproj/circuit.png" style="height: auto;max-width: 489.88px;width: 100%;" id="CircuitImg">
                    <p class="w3-medium">
<?php ParaAreaStd($AreaStore["CircuitSmartSocket"]); ?>
                    </p>
                    <p class="w3-medium">
<?php ParaAreaStd($AreaStore["CircuitPara"]); ?>
                    </p>
                </div>
                <!-- Switch Script -->
                <div class="w3-half w3-padding">
                    <h4><?php echo $AreaStore["HeadingSwitchScript"]; ?></h4>
                    <p class="w3-medium">
<?php
$para = $AreaStore["ScriptInitGPIO"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
echo str_replace(array("#", "*"), array("<samp class=\"w3-light-grey\">", "</samp>"), $nlstr);
?>    
                    </p>
                    <div style="background-color: black; color: white; width: max-content;">
                        <code>
                            gpio export 2 out<br>
                        </code>
                    </div>
                    <p>
<?php
$para = $AreaStore["ScriptReqHD"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
echo str_replace(array("#", "*"), array("<samp class=\"w3-light-grey\">", "</samp>"), $nlstr);
?>                        
                    </p>
                    <div style="background-color: black; color: white; width: max-content;">
                        <code>
                            gpio -g write 2 1<br>
                            sleep 15s<br>
                            mount /mnt/sark &amp;&gt; /home/pi/sarkmount/mnt-msg.txt<br>
                            echo &quot;mnt result: $?&quot; &gt;&gt; /home/pi/sarkmount/mnt-msg.txt<br>
                        </code>
                    </div>
                    <p>
<?php
$para = $AreaStore["ScriptFinHD"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
echo str_replace(array("#", "*"), array("<samp class=\"w3-light-grey\">", "</samp>"), $nlstr);
?>
                    </p>
                    <div style="background-color: black; color: white; width: max-content;">
                        <code>
                            sync<br>
                            sleep 10s<br>
                            umount -l /mnt/sark &amp;&gt; /home/pi/sarkmount/umnt-msg.txt<br>
                            echo &quot;umnt result: $?&quot; &gt;&gt; /home/pi/sarkmount/umnt-msg.txt<br>
                            echo -n 0 &gt; /home/pi/sarkmount/reg.txt<br>
                            gpio -g write 2 0<br>
                            echo 2<br>
                        </code>
                    </div>
                </div>
            </div>
            <!-- Web Switch Manual Interface -->
            <div style="width: 100%; float: left;">
                <h4><?php echo $AreaStore["HeadForWebIFACE"]; ?></h4>
                <img src="/img/buproj/screen-mobile-sarkman.png" id="swapp" class="w3-mobile">
                <p class="w3-medium">
<?php ParaAreaStd($AreaStore["Webswitch"]); ?>                    
                </p>
            </div>
            <!-- Backup Client Script -->
            <div style="width: 100%; float: left;">
                <h4><?php echo htmlfix($AreaStore["HeadingForClient"]); ?></h4>
                <p class="w3-medium">
<?php ParaAreaStd($AreaStore["RemoteReq"]); ?>                     
                </p>
                <div style="background-color: black; color: white; width: max-content;">
                    <code>
                        curl -X GET http://backupserver/com.php?str=reqhd &gt; bu_stat.txt
                    </code>
                </div>
                <p class="w3-medium"><?php echo $AreaStore["RemoteFin"]; ?></p>
                <div style="background-color: black; color: white; width: max-content;">
                    <code>
                        curl -X GET http://backupserver/com.php?str=finhd &gt; bu_stat.txt
                    </code>
                </div>
                <br>
                <div style="background-color: black; color: white; width: max-content;" id="remsh">
                    <code>
if [ $(cat bu_stat.txt) -eq 0 ]<br>
then<br>
&nbsp;&nbsp;&nbsp;&nbsp;rsync -rhtEl --partial --inplace --chmod=D755,F644 --delete --force $2 /media/sda1/ rsync://backupserver/bu/pi_sda1<br>
&nbsp;&nbsp;&nbsp;&nbsp;sleep 60s &amp;&amp; /root/bu_fin.sh<br>
else<br>
&nbsp;&nbsp;&nbsp;&nbsp;echo &quot;Not working&quot;<br>
fi
                    </code>
                </div>
                <p class="w3-medium" style="float: left;">
<?php
$para = $AreaStore["RemoteSync"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
echo str_replace(array("#", "*"), array("<samp class=\"w3-light-grey\">", "</samp>"), $nlstr);
?>                       
                </p>
            </div>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>