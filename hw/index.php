<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 6;
include '../php/std-head.inc';
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <style type="text/css">
        	div#lcont{
        		width: 100%;
                margin-left: unset;
        		margin-right: unset;
        	}
        	#lcont a{
        		display: block;
        	}
            #lcont > div{
                display: block;
                margin-left: unset;
                margin-right: unset;
            }
        	@media (min-width: 601px){
        		div#lcont{
        			width: 80%;
        			margin-left: 5%;
        			margin-right: 5%;
        		}
                #lcont > div{
                    display: unset;
                    margin-left: 16px;
                    margin-right: 16px;
                }
        	}
        </style>
    </head>
    <body>
<?php include '../php/std-menu-head.inc'; echo "\n";?>
		<div id="Inhalt">
			<h1 class="w3-section w3-margin-left">Hardware</h1>
			<p class="w3-padding-large">
			<?php ParaAreaStd($AreaStore["paraIntrests"]); ?>
			</p>
            <p class="w3-padding-large">
            <?php ParaAreaStd($AreaStore["pveIntrest"]); ?>
            </p>
			<div id="lcont">
<?php
$w3_colors = array("w3-teal", "w3-lime", "w3-purple", "w3-khaki", "w3-gray");
$i = 0;

$miQ = sprintf("SELECT * FROM MenuItem mi join translates t on t.MenuItem=mi.ID WHERE mi.ConnectedMenu = '%s' and (t.lang = '%s' or t.lang = 'all') ORDER BY Rank;", "hardw", $lang);
$miR = mysqli_query($DBcon, $miQ);
while ($mi = mysqli_fetch_array($miR)) {
	echo "					<div class=\"w3-quarter " . $w3_colors[$i] . "\">\n";
    $slash_inc_pos=strrpos($mi["URL"], "/") + 1;
    $here_url=substr($mi["URL"], $slash_inc_pos);
    if(is_dir($here_url) && file_exists($here_url . "/subitems.inc")){
        include $here_url . '/subitems.inc';
        if (isset($si_parent_link_root) && in_array($pid, $si_parent_link_root)){
            echo "                      <a href=\"" . urlencode($here_url) . $link_lang . "\" class=\"w3-text-white w3-xlarge\">" . htmlfix($mi["EntryText"]) . "</a>\n";
        } else {
            echo "                      <h4 class=\"w3-text-white w3-xlarge\" style=\"margin: 0; padding:0;\">" . htmlfix($mi["EntryText"]) . "</h4>\n";
        }
        unset($si_parent_link_root);
        while ($si = mysqli_fetch_array($siR)) {
            echo "                      <a href=\"" . urlencode($here_url) . "/" . urlencode($si["URL"]) . $link_lang . "\" class=\"w3-text-white w3-xlarge\" style=\"padding-left: 8px;\">" . htmlfix($si["EntryText"]) . "</a>\n";
        }
    } else {
	   echo "						<a href=\"" . urlencode($here_url) . $link_lang . "\" class=\"w3-text-white w3-xlarge\">" . htmlfix($mi["EntryText"]) . "</a>\n";
    }
	echo "					</div>\n";
	if($i < count($w3_colors)){
		$i++;
	} else {
		$i = 0;
	}
}
?>
			</div>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>