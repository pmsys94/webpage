<!DOCTYPE html>
<html>
	<head>
<?php
 $pid = 5;
include '../php/std-head.inc';
?>
		<link rel="icon" href="/pm_fav.ico">
		<title><?php echo $page["HTML_Title"]; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/styles/w3.css">
		<link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="/styles/rhg.css">
		<link rel="stylesheet" type="text/css" href="/js/pict-modal/pictmodal-style.css">
		<script type="text/javascript" src="/js/pict-modal/pictmodal.js"></script>
	</head>
	<body onload="pictmodal_init()" style="background-image: linear-gradient(to bottom, black 30%, #4080ae 80%);">
		<!-- Menu drawing -->
<?php include '../php/std-menu-head.inc'; echo "\n";?>
		<!-- Page Display Content -->
		<div id="Inhalt" style="height: 90%;">
			<!-- Header with sky backgound -->
			<header class="w3-display-container w3-text-white">
				<div class="w3-display-bottomleft">
					<h3><?php echo $AreaStore["H3_in_HeaderTag"]; ?></h3>
					<h4><?php echo $AreaStore["H4_in_HeaderTag"]; ?></h4>
				</div>
			</header>
			<!-- Container with Text's about Project and photos of find location -->
			<div style="background-color: #4080ae;" id="paraSpace" class="w3-twothird w3-text-white">
				<h3><?php echo $AreaStore["AboutProjectH3"]; ?></h3>
				<p>
<?php ParaAreaStd($AreaStore["AboutRHG"]);?>
				</p>
				<a href="https://gitlab.com/pmsys94/rhg-starto19"><?php echo $AreaStore["LinkGitlabMainCode"]; ?></a>
				<p>
<?php ParaAreaStd($AreaStore["AboutIdea"]); ?>
				</p>
				<h3><?php echo $AreaStore["PretestH3"]; ?></h3>
				<img id="imgPreTest" class="modal-image-view-img"
				 <?php echo "alt=\"" . $AreaStore["TXT_IMG_Icetetest"] . "\"" ?>
				 src="/img/rhg/Strato_Sensoren.jpg" style="width: 100%; height: auto; float: left;" onclick="imgClick(this)">
				<p>
<?php ParaAreaStd($AreaStore["Pretest"]); ?>
				</p>
				<h3><?php echo $AreaStore["AboutStartH3"]; ?></h3>
				<p>
<?php
$para = $AreaStore["AboutStart"];
$para = str_replace("#", $AreaStore["paraWord_July"], $para);
$paraEnt = htmlfix($para);
echo wordwrap(nl2br($paraEnt), 240, "\n", false) . "\n";
?>
				</p>
				<a href="https://www.robert-havemann-gymnasium.de/ballon"><?php echo $AreaStore["LinkRHGOrigProject"]; ?></a>
				<h3><?php echo $AreaStore["ParasFindLocationH3"]; ?></h3>
				<p><?php ParaAreaStd($AreaStore["ParasFindLocation"]); ?></p>
				<div class="w3-third w3-card"><img class="modal-image-view-img" alt="" src="/img/rhg/Finaler_Fundort.jpg" style="width: 100%; height: auto;" onclick="imgClick(this)"></div>
				<div class="w3-third w3-card"><img class="modal-image-view-img" alt="" src="/img/rhg/found_zoom.jpg" style="width: 100%; height: auto;" onclick="imgClick(this)"></div>
			</div>
			<!-- Graphs of recorded data -->
			<div id="graphGallery" class="w3-quarter">
				<h6 class="w3-text-white" id="hintZoom"><?php echo $AreaStore["GraphHintZoom"]; ?></h6>
				<div class="w3-card-4">
					<div class="rtl-zoom">
						<input type="checkbox" id="zoomCheck">
						<label for="zoomCheck">
							<img src="/img/rhg/Altitude.jpeg">
						</label>
					</div>
					<p>
<?php ParaAreaStd($AreaStore["GraphHeight"]); ?>					
					</p>
				</div>
				<div class="w3-card-4">
					<div class="rtl-zoom">
						<input type="checkbox" id="zoomCheck2">
						<label for="zoomCheck2">
							<img src="/img/rhg/Temperatur.JPG">
						</label>
					</div>
					<p>
<?php ParaAreaStd($AreaStore["GraphBoardTemp"]); ?>					
					</p>
				</div>
				<div class="w3-card-4">
					<div class="rtl-zoom">
						<input type="checkbox" id="zoomCheck5">
						<label for="zoomCheck5">
							<img src="/img/rhg/Luftdruck.JPG">
						</label>
					</div>
					<p>
<?php ParaAreaStd($AreaStore["Airpresure"]); ?>
					</p>
				</div>
				<div class="w3-card-4">
					<div class="rtl-zoom">
						<input type="checkbox" id="zoomCheck3">
						<label for="zoomCheck3">
							<img src="/img/rhg/Geiger-Müller-Zählrohr.jpg">
						</label>
					</div>
					<p>
<?php ParaAreaStd($AreaStore["GraphGMC"]); ?>					
					</p>
				</div>
				<div class="w3-card-4">
					<div class="rtl-zoom">
						<input type="checkbox" id="zoomCheck4">
						<label for="zoomCheck4">
							<img src="/img/rhg/UV_vier_Gläser.JPG">
						</label>
					</div>
					<p>
<?php ParaAreaStd($AreaStore["GraphUV"]); ?>					
					</p>
					<button class="w3-button w3-blue" onclick="imgClick(document.getElementById('UV-Table'))">
						UV Table mV &lt;-&gt; Index
						<img id="UV-Table" src="/img/rhg/SENS-43UV-hookup.jpg" style="display: none;" alt="UV Sensor Table Volt in UV Index">
					</button>
				</div>
			</div>
			<!-- Container for Other Flight Photos -->
			<div class="w3-text-white" id="imgGallery" style="float: left;">
<?php
$TXT_Time = htmlfix($AreaStore["TXT_Time"]);
$TXT_Altitude = htmlfix($AreaStore["TXT_Altitude"]);
?>
				<h3><?php echo $AreaStore["PictureGallery"]; ?></h3>
				<div class="w3-card-4">
					<?php
$TXT_StBallon = $AreaStore["StartedBallon"];
echo "					<img class=\"modal-image-view-img\" alt=\"" . $TXT_StBallon . "\" src=\"/img/rhg/P9940041.JPG\" onclick=\"imgClick(this)\">\n";
echo "					<p>\n";
echo "						" . $TXT_StBallon . "\n";
echo "					</p>\n"; ?>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 10:21:03 71.6m" src="/img/rhg/Bild07-06-2019_10-21-03.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 10:21:03 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 71.6</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 10:21:19 139.3m" src="/img/rhg/Bild07-06-2019_10-21-19.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 10:21:19 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 139.3</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 10:22:20 464.4m" src="/img/rhg/Bild07-06-2019_10-22-20.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 10:22:20 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 464.4</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 10:23:48 804.6m" src="/img/rhg/Bild07-06-2019_10-23-48.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 10:23:48 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 804.6</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 10:24:54 1109.2m" src="/img/rhg/Bild07-06-2019_10-24-54.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 10:24:54 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 1109.2</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 10:28:24 1109.2m" src="/img/rhg/Bild07-06-2019_10-28-24.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 10:28:24 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 1109.2</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 10:49:42 8648.7m" src="/img/rhg/Bild07-06-2019_10-49-42.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 10:49:42 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 8648.7</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 11:28:42 20113.6m" src="/img/rhg/Bild07-06-2019_11-28-42.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 11:28:42 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 20113.6</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 11:29:43 20413.2m" src="/img/rhg/Bild07-06-2019_11-29-43.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 11:29:43 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 20413.2</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 12:14:01 35673.6m" src="/img/rhg/Bild07-06-2019_12-14-01.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 12:14:01 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 35673.6</li>
					</ul>
				</div>
				<div class="w3-card-4">
					<img class="modal-image-view-img" alt="07.06.2019 12:27:16 40090.4m" src="/img/rhg/Bild07-06-2019_12-27-16.jpg" onclick="imgClick(this)">
					<ul>
						<li><?php echo $TXT_Time;?> 12:27:16 (CEST)</li>
						<li><?php echo $TXT_Altitude;?> 40090.4</li>
						<li><?php echo $AreaStore["lastPict"]; ?></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- The Modal -->
		<div id="pictmodal" class="modal">
  			<span class="close" onclick="spanClick()">&times;</span>
  			<img class="modal-content" id="modimg">
  			<div id="caption"></div>
		</div>
	</body>
</html><?php mysqli_close($DBcon); ?>
