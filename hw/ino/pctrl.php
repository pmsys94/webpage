<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 9;
include '../../php/std-head.inc';
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <style type="text/css">
            p.PFS14{
                font-size: 14px;
            }
            .PictWeb{
                width: 100%;
                height: auto;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                table-layout: fixed;
                max-width: 849px;
            }
            td.wwF{
                word-wrap: break-word;
            }
            th{
                padding: 1px !important;
                border-color: white;
            }
            td{
                padding: 1px !important;
                font-size: 12px;
            }
            div#ProtPara{
                display: none;
            }
            div#Proto{
                display: none;
            }
            input[type=radio]{
                display: none;
            }
            input[type=radio]#dpl_para:checked ~ div#ProtPara{
                display: block;
            }
            input[type=radio]#dpl_para:checked ~ label[for=dpl_para] > button{
                display: none;
            }
            input[type=radio]#dpl_proto:checked ~ div#Proto{
                display: block;
            }
            input[type=radio]#dpl_proto:checked ~ label[for=dpl_proto] > button{
                display: none;
            }
            @media (min-width: 601px){
                label{
                    display: none;
                }
                div#ProtPara{
                    display: unset;
                }
                div#Proto{
                    display: unset;
                }
                #WebCtrlTXT{
                    width: 60%;
                    float: left;
                }
                #WebCtrlImg{
                    width: 40%;
                    float: left;
                }
            }
        </style>
    </head>
    <body>
<?php
include '../../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px;">
            <h1><?php echo $AreaStore["pctrlHeader"]; ?></h1>
            <h2><?php echo $AreaStore["pctrlSubheader"]; ?></h2>
            <p>
<?php
$para = $AreaStore["HeadParaInfluences"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
$q = sprintf("SELECT EntryText, URL FROM MenuItem mi join translates t on t.MenuItem=mi.ID WHERE mi.ID = %d and (t.lang = '%s' or t.lang = 'all');", 9, $lang); // get Strato Menu Item (dep. on lang)
$r = mysqli_query($DBcon, $q);
$strato = mysqli_fetch_array($r);
$q = sprintf("SELECT EntryText, URL FROM MenuItem mi join translates t on t.MenuItem=mi.ID WHERE mi.ID = %d and (t.lang = '%s' or t.lang = 'all');", 7, $lang); // get RPI Base Menu Item (dep. on lang)
$r = mysqli_query($DBcon, $q);
$rpi = mysqli_fetch_array($r);
$q = sprintf("SELECT EntryText, URL FROM MenuItem mi join translates t on t.MenuItem=mi.ID WHERE mi.ID = %d and (t.lang = '%s' or t.lang = 'all');", 11, $lang); // get buproj submenu Item (dep. on lang)
$r = mysqli_query($DBcon, $q);
$buproj = mysqli_fetch_array($r);
$prompt_bu = "<a href=\"" . $rpi["URL"] . "/" . $buproj["URL"] . "\">&quot;" . $rpi["EntryText"] . " " . $buproj["EntryText"] . "&quot;</a>";
$prompt_sf = "<a href=\"" . $strato["URL"] . "\">&quot;" . $strato["EntryText"] . "&quot;</a>";
$paraRepl = str_replace(array("*", "#"), array($prompt_bu, $prompt_sf), $nlstr);
echo wordwrap($paraRepl, 240, "\n", false);
?>
            </p>
            <!-- HMS Task & Project Vision -->
            <div style="width: 100%; float: left;">
                <!-- HMS Task  -->
                <div class="w3-half w3-padding">
                    <h3><?php echo $AreaStore["HMS_h3"]; ?></h3>
                    <p class="PFS14">
<?php ParaAreaStd($AreaStore["HMS_Schoolproject"]); ?>
                    </p>
                </div>
                <!-- Project Vision -->
                <div class="w3-half w3-padding">
                    <h3><?php echo $AreaStore["VisionH3"]; ?></h3>
                    <p class="PFS14">
<?php ParaAreaStd($AreaStore["IdeaSmartHomeProject"]); ?>                        
                    </p>
                </div>
            </div>
            <!-- Easy Switching Prototype -->
            <div style="width: 100%; float: left;">
                <div style="float: left;" class="w3-threequarter w3-padding">
                    <h3><?php echo $AreaStore["HeadPrototype"]; ?></h3>
                    <h4><?php echo $AreaStore["ProtoMeasureH4"]; ?></h4>
                    <p>
<?php ParaAreaStd($AreaStore["pctrl_protoMeasure"]); ?>                        
                    </p>
                </div>
                <div style="float: right;" class="w3-quarter w3-padding">
                    <img src="/img/pctrl/strom_und_Spannung_Schaltplan.png" style="width: 100%;">
                    <p class="w3-tiny"><?php echo $AreaStore["PictSubsMeasure"]; ?></p>
                </div>
            </div>
            <div style="width: 100%; float: left;">
                <div style="float: left;" class="w3-threequarter w3-padding">
                    <h4><?php echo $AreaStore["ProtoSwitchH4"]; ?></h4>
                    <p>
<?php ParaAreaStd($AreaStore["SwCricuitPara"]); ?>                        
                    </p>
                </div>
                <div style="float: right;" class="w3-quarter w3-padding">
                    <img src="/img/pctrl/switchCircuit.jpg" style="width: 80%;">
                    <p class="w3-tiny"><?php echo $AreaStore["PictSubsSwSketch"]; ?></p>
                </div>
            </div>
            <!-- Remote Connect to RPi -->
            <h3><?php echo $AreaStore["RemoteRpiSection"]; ?></h3>
            <p class="w3-threequarter">
<?php ParaAreaStd($AreaStore["InoArduConnect"]); ?>
            </p>
            <div class="w3-quarter w3-padding">
                <img src="/img/pctrl/prototype_on_desk.jpg" style="width: 80%; height: auto;">
                <p class="w3-tiny">
<?php ParaAreaStd($AreaStore["PctrlSubsPrototype"]); ?>
                </p>
            </div>
            <!-- The Protocol -->
            <h3 style="float: left;"><?php echo $AreaStore["ProtcolSectionH3"]; ?></h3>
            <div style="width: 100%; float: left;">
                <input type="radio" name="protoRadio" id="dpl_para" checked="" style="float: left;">
                <label for="dpl_para" style="float: left;">
                    <button class="w3-button w3-blue" style="pointer-events: none;"><?php echo $AreaStore["dpl_Para"]; ?></button>
                </label>
                <input type="radio" name="protoRadio" id="dpl_proto" style="float: left;">
                <label for="dpl_proto" style="float: left;">
                    <button class="w3-button w3-blue" style="pointer-events: none;"><?php echo $AreaStore["dpl_Proto"]; ?></button>
                </label>
                <!-- The Text about the Serial Protocol Definition -->
                <div class="w3-half" style="float: left;" id="ProtPara">
<?php ParaAreaStd($AreaStore["PctrlProtocolPara"]); ?>
                </div>
                <!-- The Table for the Protocol Display -->
                <div class="w3-half w3-light-gray w3-padding" style="float: left;" id="Proto">
                    <h5><?php echo $AreaStore["ProtocolHeadSend"]; ?></h5>
                    <table class="w3-table w3-bordered">
                        <thead>
                            <tr class="w3-black">
                                <th><?php echo $AreaStore["TXT_ProtocolSend1"]; ?></th>
                                <th><?php echo $AreaStore["TXT_ProtocolSend2"]; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>setA</td>
                                <td><?php echo $AreaStore["TXT_ProtocolSetA"]; ?></td>
                            </tr>
                            <tr>
                                <td>setI</td>
                                <td><?php echo $AreaStore["TXT_ProtocolSetI"]; ?></td>
                            </tr>
                            <tr>
                                <td>setT &lt;TIME_NOW FORM(HH_mm_ss)&gt; &lt;TIME_TICK FORM(HH_mm)&gt; (dact | nact)</td>
                                <td><?php echo $AreaStore["TXT_ProtocolSetT"]; ?></td>
                            </tr>
                            <tr>
                                <td>info (stat | lght | powr | time)</td>
                                <td><?php echo $AreaStore["TXT_ProtocolInfo"]; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <h5><?php echo $AreaStore["ProtocolHeadRecv"]; ?></h5>
                    <table class="w3-table w3-bordered">
                        <thead>
                            <tr class="w3-black">
                                <th><?php echo $AreaStore["TXT_ProtocolRecv1"]; ?></th>
                                <th><?php echo $AreaStore["TXT_ProtocolRecv2"]; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>done</td>
                                <td><?php echo $AreaStore["TXT_ProtocolDone"]; ?></td>
                            </tr>
                            <tr>
                                <td>secf</td>
                                <td><?php echo $AreaStore["TXT_ProtocolSecf"]; ?></td>
                            </tr>
                            <tr>
                                <td>echo devc (iact | nact)</td>
                                <td><?php echo $AreaStore["TXT_ProtocolEcho1"]; ?></td>
                            </tr>
                            <tr>
                                <td>echo lght (nght | dayL | dayS | dayC)</td>
                                <td><?php echo $AreaStore["TXT_ProtocolEcho2"]; ?></td>
                            </tr>
                            <tr>
                                <td>echo powr &lt;var&gt;</td>
                                <td><?php echo $AreaStore["TXT_ProtocolEcho3"]; ?></td>
                            </tr>
                            <tr>
                                <td>echo time (iact | nact) + &lt;HH_MM&gt;</td>
                                <td><?php echo $AreaStore["TXT_ProtocolEcho4"]; ?></td>
                            </tr>
                            <tr>
                                <td>fail</td>
                                <td><?php echo $AreaStore["TXT_ProtocolFail"]; ?></td>
                            </tr>
                            <tr>
                                <td>ncom</td>
                                <td><?php echo $AreaStore["TXT_ProtocolNcom"]; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- The Webcontrol -->
            <div style="float: left;">
                <div id="WebCtrlTXT" class="w3-mobile">
                    <h3><?php echo $AreaStore["PctrlWebcontrolH3"]; ?></h3>
                    <p>
<?php ParaAreaStd($AreaStore["PctrlWebControl"]); ?>
                    </p>
                </div>
                <div id="WebCtrlImg" class="w3-mobile">
                    <img src="/img/pctrl/Screenshot_2016-03-04-19-38-15.png" class="PictWeb" style="max-width: 200px;">
                    <img src="/img/pctrl/Screenshot Desktop.PNG" class="PictWeb" style="max-width: 300px;">
                    <p class="w3-tiny" style="display: block;">
<?php ParaAreaStd($AreaStore["PctrlSubsWeb"]); ?>
                    </p>
                </div>
            </div>
            <!-- Never Developed & Future Plans -->
            <div style="width: 100%; float: left;">
                <div style="width: 100%; display: block;">
                    <h3><?php echo $AreaStore["pctrlPresentPublicH3"]; ?></h3>
                    <p>
<?php ParaAreaStd($AreaStore["pctrlPresentPublicP"]); ?>
                    </p>
                </div>
                <!-- Never Developed Features -->
                <div class="w3-half" style="padding-right: 5px;">
                    <h4><?php echo $AreaStore["NonDevH4"]; ?></h4>
                    <p>
<?php ParaAreaStd($AreaStore["NonDevPara"]); ?>
                    </p>
                </div>
                <!-- Future Plans -->
                <div class="w3-half" style="padding-left: 5px;">
                    <h4><?php echo $AreaStore["PctrlFutureH4"]; ?></h4>
                    <p>
<?php ParaAreaStd($AreaStore["PctrlFuturePara"]); ?>
                    </p>
                </div>
            </div>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>