<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 23;
include '../../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, -16); // Also include common pveCloud
$AreaStore += pullAreas($DBcon, $lang, 0); // Also include dummy page
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <link rel="stylesheet" type="text/css" href="/js/pict-modal/pictmodal-style.css">
        <script type="text/javascript" src="/js/pict-modal/pictmodal.js"></script>
        <style type="text/css">
            samp{
                font-size: 12px;
                line-height: 0;
                word-break: keep-all;
            }
            code{
                font-size: 12px;
                line-height: 0;
            }
            .code-box {
                background-color: black;
                color: white;
                width: max-content;
            }
            .flex-container{
                display: flex;
                flex-wrap: wrap;
            }
            .grid-cell{
                margin: 5px;
                float: left;
                width: 99%;
                max-width: 31.89%;
                min-width: 200px;
                background-color: white;
            }
            .grid-container{
                width: 100%;
                max-width: 1600px;
                margin: auto;
            }
            @media (min-width: 600px) {
                b.bash-br {
                    display: none;
                }
            }
            @media (max-width: 600px) {
                .grid-cell {
                    max-width: unset;
                    min-width: unset;
                    width: 100%;
                }
            }
        </style>
    </head>
    <body onload="pictmodal_init()">
<?php
include '../../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px; padding-right: 10px;">
            <h1><?php echo $AreaStore["Project_PVE_Cloud"]; ?></h1>
            <h2><?php echo $AreaStore["InstallTopic"]; ?></h2>
            <p>
<?php ParaAreaStd($AreaStore["InstallIntro"]); ?>
            </p>
            <!-- Preconditions -> VM shut off -->
            <h3><?php echo $AreaStore["Preconditions"]; ?></h3>
            <p>
<?php ParaAreaStd($AreaStore["Pre_VMoff"]); ?>
            </p>
            <h3><?php echo $AreaStore["CopingImage"]; ?></h3>
            <p>
<?php ParaAreaStd($AreaStore["Transfer2Actions"]); ?>
            </p>
            <!-- Server reboot rescue os -->
            <h4><?php echo $AreaStore["RebootAtStrato"]; ?></h4>
            <p>
<?php ParaAreaStd($AreaStore["GoToResue"]); ?>
            </p>
            <p class="w3-text-red">
<?php ParaAreaStd($AreaStore["HintKeepOriginal"]); ?>
            </p>
            <h4><?php echo $AreaStore["ImgConvPush"]; ?></h4>
            <p>
<?php ParaAreaStd($AreaStore["Conv_and_Upload"]); ?>
            </p>
            <h5><?php echo $AreaStore["VBOX_ImgConvert"]; ?></h5>
            <!-- a) vbox clone vhd-to-img -->
            <div style="width: 99.5%; display: block; overflow: auto;">
                <div class="w3-threequarter">
                    <p>
<?php
$para = $AreaStore["VBOX_Convert"];
$cmd = "*cd C:\\Program Files\\Oracle\\VirtualBox\\#";
$repl_str = str_replace('+', $cmd, $para);
$entity = htmlfix($repl_str);
$nlstr = nl2br($entity);
$repl_str = str_replace(array('*', '#'), array('<samp class="w3-light-grey">', '</samp>'), $nlstr);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
                    </p>
                    <div class="code-box">
                        <code>
                            VBoxManage clonemedium {d0765395-081b-4fe2-a31a-32b73cc65269} <b class="bash-br">&bsol;<br></b>pveProv.img --format RAW
                        </code>
                    </div>
                </div>
<?php
echo "                <div class=\"w3-quarter\">\n";
echo "                    <p class=\"w3-tiny\" style=\"display: block; width: 100%;\">" . $AreaStore["Click2Zoom"] . "</p>\n";
echo "                    <div style=\"width: 100%; display: block;\">\n";
echo "                        <img src=\"/img/pveCloud/05_install/vbox-diskGUID-MenuSelect.png\" style=\"width: 100%;\" alt=\"" . htmlfix($AreaStore["IMG0_Caption"]) . "\"  onclick=\"imgClick(this)\">\n";
echo "                        <p class=\"w3-small\">\n";
echo htmlfix($AreaStore["IMG0_Caption"]);
echo "                        </p>\n";
echo "                    </div>\n";
echo "                    <div style=\"width: 100%; display: block;\">\n";
echo "                        <img src=\"/img/pveCloud/05_install/vbox-diskGUID-View.png\" style=\"width: 100%;\" alt=\"" . htmlfix($AreaStore["IMG1_Caption"]) . "\"  onclick=\"imgClick(this)\">\n";
echo "                        <p class=\"w3-small\">\n";
echo htmlfix($AreaStore["IMG1_Caption"]);
echo "                        </p>\n";
echo "                    </div>\n";
echo "                </div>\n";
?>
            </div>
            <h5><?php echo $AreaStore["ImgPush"]; ?></h5>
            <!-- b) push img over ssh to server with dd -->
            <p>
<?php
$para = $AreaStore["ImagSSHPipe"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$repl_str = str_replace(array('*', '#'), array('<samp class="w3-light-grey">', '</samp>'), $nlstr);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
            </p>
            <div class="code-box">
                <code>
                    dd if&equals;pveProv.img status&equals;progress | gzip -c -9 | ssh stratoserver <b class="bash-br">&bsol;<br></b>'gzip -d | dd of&equals;/dev/sda'
                </code>
            </div>
            <h3><?php echo $AreaStore["PostSetup"]; ?></h3>
            <p>
<?php ParaAreaStd($AreaStore["NextServerSteps"]); ?>
            </p>
            <h4><?php echo $AreaStore["DiskResize"]; ?></h4>
            <!-- lvm resize  && reboot -->
            <div style="width: 99.5%; display: block; overflow: auto;">
                <div class="w3-threequarter">
                    <p>
<?php
$para = $AreaStore["ResizeRootFS"];
$plus_cmd = "*vgchange -a n#";
$tilde_cmd = "*fdisk /dev/sda#";
$repl_str = str_replace('+', $plus_cmd, $para);
$repl_str = str_replace('~', $tilde_cmd, $repl_str);
$entity = htmlfix($repl_str);
$nlstr = nl2br($entity);
$repl_str = str_replace(array('*', '#'), array('<samp class="w3-light-grey">', '</samp>'), $nlstr);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
                    </p>
                    <div class="code-box">
                        <code>
                            lvextend -r -l 100&percnt;FREE /dev/pve/root
                        </code>
                    </div>
                </div>
<?php
echo "                <div class=\"w3-quarter\">\n";
echo "                    <p class=\"w3-tiny\" style=\"display: block; width: 100%;\">" . $AreaStore["Click2Zoom"] . "</p>\n";
echo "                    <div style=\"width: 100%; display: block;\">\n";
echo "                        <img src=\"/img/pveCloud/05_install/fdisk-protest-disksize.png\" style=\"width: 100%;\" alt=\"" . htmlfix($AreaStore["IMG2_Caption"]) . "\"  onclick=\"imgClick(this)\">\n";
echo "                        <p class=\"w3-small\">\n";
echo htmlfix($AreaStore["IMG2_Caption"]);
echo "                        </p>\n";
echo "                    </div>\n";
echo "                    <div style=\"width: 100%; display: block;\">\n";
echo "                        <img src=\"/img/pveCloud/05_install/resize-fdisk0.png\" style=\"width: 100%;\" alt=\"" . htmlfix($AreaStore["IMG3_Caption"]) . "\"  onclick=\"imgClick(this)\">\n";
echo "                        <p class=\"w3-small\">\n";
echo htmlfix($AreaStore["IMG3_Caption"]);
echo "                        </p>\n";
echo "                    </div>\n";
echo "                </div>\n";
?>
            </div>
            <h4><?php echo $AreaStore["FixupConfig"]; ?></h4>
            <!-- mount root && reconfigure a) b) c) d) -->
            <p>
<?php ParaAreaStd($AreaStore["RechonfigChroot"]); ?>
            </p>
            <div class="code-box" id="chroot-cmd" style="margin: auto;">
                <code>
                    mount /dev/pve/root /mnt<br>
                    mount --bind /dev /mnt/dev<br>
                    mount --bind /dev/pts /mnt/dev/pts<br>
                    mount --bind /sys /mnt/sys<br>
                    mount --bind /proc /mnt/proc<br>
                    mount --bind /run /mnt/run<br>
                    chroot /mnt /bin/bash<br>
                </code>
            </div>
            <div class="w3-container grid-container">
                <div class="flex-container">
                    <!-- Interface def change -->
                    <div class="w3-card-4 grid-cell">
                        <h5><?php echo $AreaStore["ModInterfaces"]; ?></h5>
                        <p class="w3-small">
<?php
$link_query = sprintf("SELECT URL FROM MenuItem mi join translates t on t.MenuItem=mi.ID WHERE mi.ID = 18 and t.lang = '%s'", $lang);
$link_res = mysqli_query($DBcon, $link_query);
$link_02_data = mysqli_fetch_array($link_res);
$para = $AreaStore["SetServerIface"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$repl_str = str_replace(array('*', '#'), array('<a href="' . $link_02_data["URL"] . '">', '</a>'), $nlstr);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
                        </p>
                        <p class="w3-small w3-text-teal"><?php echo htmlfix($AreaStore["Change_From"]) . "<br>"; ?></p>
                        <div class="code-box">
                            <code>
                                auto enp0s3<br>
                                iface enp0s3 inet static<br>
                                &nbsp;&nbsp;&nbsp;&nbsp;address 192.168.55.4/24<br>
                                &nbsp;&nbsp;&nbsp;&nbsp;gateway 192.168.55.1<br>
                            </code>
                        </div>
                        <p class="w3-small w3-text-green"><?php echo htmlfix($AreaStore["Change_To"]) . "<br>"; ?></p>
                        <div class="code-box">
                            <code>
                                auto eno1<br>
                                iface eno1 inet static<br>
                                &nbsp;&nbsp;&nbsp;&nbsp;address 85.214.5.42/32<br>
                                &nbsp;&nbsp;&nbsp;&nbsp;gateway 85.214.254.42<br>
                            </code>
                        </div>
                    </div>
                    <!-- Fixup Hostname -->
                    <div class="w3-card-4 grid-cell">
                        <h5><?php echo $AreaStore["FixupHostname"] ?></h5>
                        <p class="w3-small">
<?php
$para = $AreaStore["UpdateHostname"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$repl_str = str_replace(array('*', '#'), array('<a href="' . $link_02_data["URL"] . '">', '</a>'), $nlstr);
$repl_str = str_replace(array('+', '~'), array('<samp class="w3-light-grey">', '</samp>'), $repl_str);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
                        </p>
                    </div>
                    <!-- Serial Console activate now -->
                    <div class="w3-card-4 grid-cell">
                        <h5><?php echo $AreaStore["EnableSerCons"] ?></h5>
                        <p class="w3-small">
<?php
$link_query = sprintf("SELECT URL FROM MenuItem mi join translates t on t.MenuItem=mi.ID WHERE mi.ID = 20 and t.lang = '%s'", $lang);
$link_res = mysqli_query($DBcon, $link_query);
$link_04_data = mysqli_fetch_array($link_res);
$para = $AreaStore["RenSerCons"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$repl_str = str_replace(array('*', '#'), array('<a href="' . $link_04_data["URL"] . '">', '</a>'), $nlstr);
$repl_str = str_replace(array('+', '~'), array('<samp class="w3-light-grey">', '</samp>'), $repl_str);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- restart into pve on server -->
            <p>
<?php
$para = $AreaStore["UmountRestart"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$repl_str = str_replace(array('+', '~'), array('<samp class="code-box">', '</samp>'), $nlstr);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
            </p>
            <h3><?php echo $AreaStore["ExtraHarddisk"]; ?></h3>
            <!-- += hdd - setup zfs mirrored -->
            <p>
<?php ParaAreaStd($AreaStore["HDD_additional"]); ?>
            </p>
            <h4><?php echo $AreaStore["FormatZFS"]; ?></h4>
<?php
echo "            <div style=\"width: 99.5%; display: block; overflow: auto;\">\n";
echo "                <div class=\"w3-threequarter\">\n";
echo "                    <p>\n";
ParaAreaStd($AreaStore["Del_MD_RAID"]);
echo "                    </p>\n";
echo "                    <p>\n";
ParaAreaStd($AreaStore["mkZFS"]);
echo "                    </p>\n";
echo "                </div>\n";
echo "                <div class=\"w3-quarter\">\n";
echo "                    <p class=\"w3-tiny\" style=\"display: block; width: 100%;\">" . $AreaStore["Click2Zoom"] . "</p>\n";
echo "                    <div style=\"width: 100%; display: block;\">\n";
echo "                        <img src=\"/img/pveCloud/05_install/pve-HDD.png\" style=\"width: 100%;\" alt=\"" . htmlfix($AreaStore["IMG4_Caption"]) . "\"  onclick=\"imgClick(this)\">\n";
echo "                        <p class=\"w3-small\">\n";
echo htmlfix($AreaStore["IMG4_Caption"]);
echo "                        </p>\n";
echo "                    </div>\n";
echo "                    <div style=\"width: 100%; display: block;\">\n";
echo "                        <img src=\"/img/pveCloud/05_install/pveAddZFS.png\" style=\"width: 100%;\" alt=\"" . htmlfix($AreaStore["IMG5_Caption"]) . "\"  onclick=\"imgClick(this)\">\n";
echo "                        <p class=\"w3-small\">\n";
echo htmlfix($AreaStore["IMG5_Caption"]);
echo "                        </p>\n";
echo "                    </div>\n";
echo "                </div>\n";
echo "            </div>\n";
?>
		</div>
        <!-- The Modal -->
        <div id="pictmodal" class="modal">
            <span class="close" onclick="spanClick()">&times;</span>
            <img class="modal-content" id="modimg">
            <div id="caption"></div>
        </div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>
