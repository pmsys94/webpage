<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 21;
include '../../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, -16); // Also include common pveCloud
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <style type="text/css">
            code{
                font-size: 12px;
                line-height: 0;
            }
            .code-box {
                background-color: black;
                color: white;
                width: max-content;
            }
        </style>
    </head>
    <body>
<?php
include '../../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px; padding-right: 10px;">
            <h1><?php echo $AreaStore["Project_PVE_Cloud"]; ?></h1>
            <h2><?php echo $AreaStore["FW_Topic"]; ?></h2>
            <p>
<?php ParaAreaStd($AreaStore["Firewall_on_PVE"]); ?>
            </p>
            <!-- World open ports on Proxmox -->
            <h3><?php echo $AreaStore["WO_Ports"]; ?></h3>
            <p>
<?php
$para = $AreaStore["PVE_Ports"];
$entity = htmlfix($para);
$nl_str = nl2br($entity);
$repl_str = str_replace(array("*", "~"), array("<a href=\"https://pve.proxmox.com/wiki/Firewall#_ports_used_by_proxmox_ve\">", "</a>"), $nl_str);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
            </p>
            <!-- Why custom firewall use for ports -->
            <h3><?php echo $AreaStore["WhyExtraFW"]; ?></h3>
            <p>
<?php ParaAreaStd($AreaStore["NeedUFW"]); ?>
            </p>
            <!-- ufw rules & pict installed rules -->
            <h3><?php echo $AreaStore["SetupUFW"]; ?></h3>
            <!-- UFW intro -->
            <p>
<?php
$para = $AreaStore["InstallUFW"];
$entity = htmlfix($para);
$nl_str = nl2br($entity);
$repl_str = str_replace(array("*", "#"), array("<samp class=\"code-box\">", "</samp>"), $nl_str);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
            </p>
            <!-- UFW settings (shell code + pict) -->
            <h4><?php echo $AreaStore["UFW_Rules"]; ?></h4>
            <p>
<?php
$para = $AreaStore["AddingUFWrules"];
$entity = htmlfix($para);
$nl_str = nl2br($entity);
$repl_str = str_replace(array("+", "$"), array("<samp class=\"code-box\">", "</samp>"), $nl_str);
$repl2_str = str_replace(array("*", "#"), array("<b>", "</b>"), $repl_str);
echo wordwrap($repl2_str, 240, "\n", false) . "\n";
?>
            </p>
            <div id="pict-blocker" style="width: 100%; display: block; overflow: auto;">
                <div class="code-box" style="float: left; margin: 5px">
                    <code>
                        ufw logging off<br>
                        ufw default allow routed<br>
                        ufw default allow incoming<br>
                        ufw allow from 127.0.0.1 to 127.0.0.1 port 8006<br>
                        ufw deny to 192.168.55.4 port 8006<br>
                        ufw deny to 85.214.5.42 port 8006<br>
                        ufw allow from 127.0.0.1 to 127.0.0.1 port 3128<br>
                        ufw deny to 192.168.55.4 port 3128<br>
                        ufw deny to 85.214.5.42 port 3128<br>
                        ufw deny to 192.168.55.4 port 111<br>
                        ufw deny to 85.214.5.42 port 111
                    </code>
                </div>
                <div style="float: left; max-width: 504px; width: 99%; margin: 5px;">
                    <img src="/img/pveCloud/03_firewall/ufw-table.png" style="width: 100%">
                    <p>
                        <?php echo $AreaStore["PICT_ufw-table"]; ?>
                    </p>
                </div>
            </div>
            <!-- enable fw and whats then -->
            <h4><?php echo $AreaStore["ActivateFW"]; ?></h4>
            <p>
<?php
$para = $AreaStore["EnableFW"];
$entity = htmlfix($para);
$nl_str = nl2br($entity);
$repl_str = str_replace(array("*", "#"), array("<samp class=\"code-box\">", "</samp>"), $nl_str);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
            </p>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>
