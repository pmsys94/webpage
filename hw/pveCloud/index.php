<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 16;
include '../../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, -16); // Also include common pveCloud
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <style type="text/css">
            .flex-container{
                display: flex;
                flex-wrap: wrap;
            }
            @media (min-width: 600px){
                #txtFacts{
                    float: left;
                }
                #imgFacts{
                    float: right;
                }
            }
        </style>
    </head>
    <body>
<?php
include '../../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px; padding-right: 10px;">
            <h1><?php echo $AreaStore["Project_PVE_Cloud"]; ?></h1>
            <!-- Intro Para -->
            <div>
                <h2><?php echo $AreaStore["basics"]; ?></h2>
                <p>
<?php
$para = $AreaStore["Basic_PVE_interests"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
$repl = str_replace(array("*", "#"), array("<a href=\"https://proxmox.com/en/\">", "</a>"), $nlstr);
echo wordwrap($repl, 240, "\n", false) . "\n";
?>
                </p>
                <p>
<?php
$para = $AreaStore["pveStratoInterst"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
$repl = str_replace(array("*", "#"), array("<a href=\"https://www.strato.de/server/linux-dedicated-server/\">", "</a>"), $nlstr);
echo wordwrap($repl, 240, "\n", false) . "\n";
?>
                </p>
            </div>
            <!-- My Server -->
            <div>
                <h3><?php echo $AreaStore["MyServer"]; ?></h3>
                <h4><?php echo $AreaStore["ServerPackets"]; ?></h4>
                <div>
                    <p>
<?php ParaAreaStd($AreaStore["AboutServerPackets"]); ?>
                    </p>
                </div>
                <!-- Fact STRATO Server Offers -->
                <div id="txtFacts">
                    <h4><?php echo $AreaStore["MyMachine"]; ?></h4>
                    <div class="w3-third" id="imgFacts">
                        <img class="w3-img w3-mobile" style="width: 100%; margin-left: 10px; max-width: 283.15px;" src="/img/pveCloud/basics/stratoserver-metainfo.png" alt="my-server meta facts">
                    </div>
                    <p>
<?php ParaAreaStd($AreaStore["AboutMyServer"]); ?>
                    </p>
                </div>
            </div>
            <!-- Special Information about Server from STRATO -->
            <div>
                <h3><?php echo $AreaStore["SpecialsMachines"]; ?></h3>
                <div class="w3-container">
                    <div class="flex-container">
                        <div class="w3-half w3-card-4 w3-padding">
                            <h4><?php echo $AreaStore["PVE_BA_SP_Net"]; ?></h4>
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["stratoNetwork"]); ?>
                            </p>
                        </div>
                        <div class="w3-half w3-card-4 w3-padding">
                            <h4><?php echo $AreaStore["PVE_BA_SP_OS"]; ?></h4>
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["stratoOwnOS"]); ?>
                            </p>
                        </div>
                    </div>
                    <div class="flex-container">
                        <div class="w3-half w3-card-4 w3-padding">
                            <h4><?php echo $AreaStore["PVE_BA_SP_Resc"]; ?></h4>
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["stratoRescueOS"]); ?>
                            </p>
                        </div>
                        <div class="w3-half w3-card-4 w3-padding">
                            <h4><?php echo $AreaStore["PVE_BA_SP_SerCon"]; ?></h4>
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["ServerSerialConsole"]); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Install a Proxmox on a strato server -->
            <div>
                <h3><?php echo $AreaStore["pve2strato_Inst"]; ?></h3>
                <p>
<?php ParaAreaStd($AreaStore["ThinkingsPVEinstall"]); ?>
                </p>
            </div>
            <!-- links to subsites -->
            <div class="w3-container w3-card-2 w3-padding" style="margin: 10px;">
<?php
$w3_colors = array("w3-teal", "w3-lime", "w3-purple", "w3-khaki", "w3-gray");
$i = 0;
$miQ = sprintf("SELECT * FROM MenuItem mi join translates t on t.MenuItem=mi.ID WHERE mi.ConnectedMenu = '%s' and (t.lang = '%s' or t.lang = 'all') ORDER BY Rank;", "pveCloud", $lang);
$miR = mysqli_query($DBcon, $miQ);
while ($mi = mysqli_fetch_array($miR)) {
    echo "                  <div class=\"w3-quarter " . $w3_colors[$i] . "\">\n";
    $slash_inc_pos=strrpos($mi["URL"], "/");
    $here_url=substr($mi["URL"], $slash_inc_pos);
    if(is_dir($here_url) && file_exists($here_url . "/subitems.inc")){
        echo "                      <h4 class=\"w3-text-white w3-xlarge\" style=\"margin: 0; padding:0;\">" . htmlfix($mi["EntryText"]) . "</h4>\n";
        include $here_url . '/subitems.inc';
        while ($si = mysqli_fetch_array($siR)) {
            echo "                      <a href=\"" . urlencode($here_url) . "/" . urlencode($si["URL"]) . $link_lang . "\" class=\"w3-text-white w3-xlarge\" style=\"padding-left: 8px;\">" . htmlfix($si["EntryText"]) . "</a>\n";
        }
    } else {
       echo "                       <a href=\"" . urlencode($here_url) . $link_lang . "\" class=\"w3-text-white w3-xlarge\">" . htmlfix($mi["EntryText"]) . "</a>\n";
    }
    echo "                  </div>\n";
    if($i+1 < count($w3_colors)){
        $i++;
    } else {
        $i = 0;
    }
}
?>
            </div>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>