<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 24;
include '../../../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, 0); // Also include dummy page
$AreaStore += pullAreas($DBcon, $lang, -16); // Also include common pveCloud
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <link rel="stylesheet" type="text/css" href="/styles/pveCloud/pveMixed/nsvr.css">
        <link rel="stylesheet" type="text/css" href="/styles/pveCloud/pveMixed/pveUg.css">
        <style type="text/css">
            .flex-container{
                display: flex;
                flex-wrap: wrap;
            }
            div#Inhalt .w3-card-2, div#Inhalt .w3-card-4 {
                margin: 5px;
            }
            input[type=radio]{
                display: none;
            }
            label, div.collapsible {
                float: left;
            }
            label {
                margin-left: 5px;
                margin-bottom: 5px;
                background-color: gray;
                color: white;
            }
            label:hover {
                color: red;
            }
        </style>
    </head>
    <body>
<?php
include '../../../php/std-menu-head.inc';
echo "\n";
include '../subitems.inc';
/* Because of change into a directory we need to fixup $si_root
as it belongs to dir above.*/
$slash_pos=strrpos($si_root, "/");
$si_root=substr($si_root, 0, $slash_pos);
include '../../../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px; padding-right: 10px;">
            <h1><?php echo $AreaStore["Project_PVE_Cloud"]; ?></h1>
            <!-- Intro Para -->
            <div>
                <h2><?php echo $AreaStore["MutilpleTopics"]; ?></h2>
                <p>
<?php ParaAreaStd($AreaStore["AboutPage"]); ?>
                </p>
            </div>
            <p><?php echo $AreaStore["WordTopicChoice"]; ?></p>
            <input type="radio" name="dispDiv" id="dispDiv_nsvr" checked="">
            <input type="radio" name="dispDiv" id="dispDiv_pveUg">
            <label for="dispDiv_nsvr"><?php echo $AreaStore["NSVR_Headline"]; ?></label>
            <label for="dispDiv_pveUg"><?php echo $AreaStore["TP_UpgradePVE"]; ?></label>
            <!-- STRATO New server ordered -->
<?php include 'nsvr.inc'; ?>
            <!-- PVE Upgrages -->
<?php include 'pveUg.inc'; ?>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>