            <div class="w3-container w3-card-2 collapsible" id="pveUg_content">
                <h3><?php echo $AreaStore["TP_UpgradePVE"]; ?></h3>
                <p>
<?php ParaAreaStd($AreaStore["PVE_UgIntro"]); ?>
                </p>
                <a href="https://pve.proxmox.com/wiki/Upgrade_from_7_to_8"><?php echo $AreaStore["SeeMore"] ?></a><br>
                <?php echo $AreaStore["PVE_UgBasicsOL"] . "<br>\n";
echo "                <ol>\n";
echo "                    <li>" . $AreaStore["pveUg_Basics_Backups"] . "</li>\n";
echo "                    <li>" . $AreaStore["pveUg_Basics_XtoY"] . "</li>\n";
echo "                    <li>" . $AreaStore["pveUg_Basics_AptUg"] . "</li>\n";
echo "                    <li>" . $AreaStore["pveUg_Basics_AptList"] . "</li>\n";
echo "                    <li>" . $AreaStore["pveUg_Basics_AptRef"] . "</li>\n";
echo "                    <li>" . $AreaStore["pveUg_Basics_Upgrade"] . "</li>\n";
echo "                    <li>" . $AreaStore["pveUg_Basics_Cleanup"] . "</li>\n";
echo "                </ol>\n";
echo "                <p>\n";
ParaAreaStd($AreaStore["pveUg_SomeQuestions"]);
echo "\n                </p>\n";
echo "                <ul>\n";
echo "                    <li>" . $AreaStore["pveUg_APT_OurAndPVE"] . "</li>\n";
echo "                    <li>" . $AreaStore["pveUg_APT_OurNoPVE"] . "</li>\n";
echo "                    <li>" . $AreaStore["pveUg_APT_OnlyPVE"] . "</li>\n";
echo "                    <li>" . $AreaStore["pveUg_APT_NoOneCh"] . "</li>\n";
echo "                </ul>\n";
echo "                <p>\n";
ParaAreaStd($AreaStore["pveUg_HintsFromHere"]);
echo "\n                <p>\n";
?>
                <div class="flex-container">
                    <div class="w3-container w3-card-4 pm-dual-601 pm-dual-801">
                        <h4><?php echo $AreaStore["pveUg_Conf_SSH"]; ?></h4>
                        <p>
<?php
$para = $AreaStore["pveUg_SSHD_conf"];
$fix_str = htmlfix($para);
$repl_str = str_replace(array("*", "|"), array("<a href=\"/hw/pveCloud/02_Net_SSH.php#sshd_diff\">", "</a>"), $fix_str);
$brStr = nl2br($repl_str);
echo wordwrap($brStr, 240, "\n", false) . "\n";
?>
                        </p>
                    </div>
                    <div class="w3-container w3-card-4 pm-dual-601 pm-dual-801">
                        <h4><?php echo $AreaStore["pveUg_Grub_Default"]; ?></h4>
                        <p>
<?php ParaAreaStd($AreaStore["pveUg_LooseSerial"]); ?>
                        </p>
                    </div>
                    <div class="w3-container w3-card-4 pm-dual-601 pm-dual-801">
                        <h4><?php echo $AreaStore["pveUg_GrubInstLoc"]; ?></h4>
                        <p>
<?php ParaAreaStd($AreaStore["pveUg_GrubFirstUg"]); ?>
                        </p>
                    </div>
                    <div class="w3-container w3-card-4 pm-dual-601 pm-dual-no">
                        <h4><?php echo $AreaStore["pveUg_BU_Rescue"]; ?></h4>
                        <p>
<?php
$para = $AreaStore["pveUg_UseRescue"];
$fix_str = htmlfix($para);
$repl_str = str_replace(array("*", "|"), array("<a href=\"/hw/pveCloud/05_install_server.php#chroot-cmd\">", "</a>"), $fix_str);
$brStr = nl2br($repl_str);
echo wordwrap($brStr, 240, "\n", false) . "\n";
?>
                        </p>
                    </div>
                </div>
            </div>