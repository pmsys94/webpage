            <div class="w3-container w3-card-2 collapsible" id="nsvr_content">
                <h3><?php echo $AreaStore["NSVR_Headline"]; ?></h3>
                <p>
<?php ParaAreaStd($AreaStore["NSVR_Intro"]); ?>
                </p>
                <div class="flex-container">
                    <!-- vgl old server -->
                    <div class="w3-container w3-card-4 w3-half w3-half-margin">
                        <div class="pm-half-801 w3-third-flr">
                            <img src="/img/pveCloud/stratoserver-metainfo-1st.png" id="nsvr_img_old" style="width: 100%;">
                        </div>
                        <h4><?php echo $AreaStore["NSVR_OldSetup"]; ?></h4>
                        <p>
<?php ParaAreaStd($AreaStore["NSVR_OldSvrFacts"]); ?>
                        </p>
                    </div>
                    <!-- vgl new server -->
                    <div class="w3-container w3-card-4 w3-half w3-half-margin">
                        <div class="pm-half-801 w3-third-flr">
                            <img src="/img/pveCloud/basics/stratoserver-metainfo.png" id="nsvr_img_new" style="width: 100%;">
                        </div>
                        <h4><?php echo $AreaStore["NSVR_NewSetup"]; ?></h4>
                        <p>
<?php ParaAreaStd($AreaStore["NSVR_NewSvrFacts"]); ?>
                        </p>
                    </div>
                </div>
                <h4><?php echo $AreaStore["NSVR_SSD_Change"]; ?></h4>
                <p>
<?php ParaAreaStd($AreaStore["NSRV_MergeDataParts"]); ?>
                </p>
                <h4><?php echo $AreaStore["NSVR_MovePVE2New"]; ?></h4>
                <p>
<?php ParaAreaStd($AreaStore["NSVR_MovePVE"]); ?>
                </p>
            </div>