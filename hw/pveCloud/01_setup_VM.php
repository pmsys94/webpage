<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 17;
include '../../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, -16); // Also include common pveCloud
$AreaStore += pullAreas($DBcon, $lang, 0); // Also include dummy page
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <style type="text/css">
            code{
                font-size: 12px;
                line-height: 0;
            }
            input[type=checkbox] {
                display: none;
            }
            input[type=checkbox]:checked ~ label > img {
                transform: scale(2.5) translate(-50px, -25px);
                cursor: zoom-out;
            }
            .rtl-zoom img {
                transition: transform 0.25s ease;
                cursor: zoom-in;
            }
            div.rtl-zoom {
                margin-top: 10px;
            }
            label {
                pointer-events: none;
            }
            p#C2Zoom {
                display: none;
            }
            @media (min-width: 601px) {
                label {
                    pointer-events: unset;
                }
                p#C2Zoom {
                    display: unset;
                }
            }
            @media (max-width: 1079px) {
                .mobile1000 {
                    width: 100%;
                }
            }
            @media (min-width: 1080px) and (max-width: 1180px) {
                .mobile-Wmax370 {
                    max-width: 370px;
                }
                .smallTabStacked {
                    width: 100%;
                }
            }
        </style>
    </head>
    <body>
<?php
include '../../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px; padding-right: 10px;">
            <h1><?php echo $AreaStore["Project_PVE_Cloud"]; ?></h1>
            <!-- Intro Para -->
            <div>
                <h2><?php echo $AreaStore["prepAndVMs"]; ?></h2>
                <p>
<?php
$para = $AreaStore["ThnkingsPrepAndVM"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
$repl =  str_replace(array("*", "#"), array("<a href=\"./" . $link_lang . "\">", "</a>"), $nlstr);
echo wordwrap($repl, 240, "\n", false) . "\n";
?>
                </p>
            </div>
            <!-- Mainly PVE Install and accessing -->
            <div>
                <h3><?php echo $AreaStore["Normal_PVE_install"]; ?></h3>
                <p>
<?php ParaAreaStd($AreaStore["PVE_install_mainly"]); ?>
                </p>
                <h4><?php echo $AreaStore["strato_Mod_Deb"]; ?></h4>
                <p>
<?php
$para = $AreaStore["stratoAlterInstall"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
$repl = str_replace(array("*", "#"), array("<a href=\"https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_12_Bookworm\">", "</a>"), $nlstr);
echo wordwrap($repl, 240, "\n", false) . "\n";
?>
                </p>
                <h4><?php echo $AreaStore["startoInstallREQ"]; ?></h4>
                <ul>
                    <li><?php echo $AreaStore["stratoREQ_ISO"]; ?></li>
                    <li><?php echo $AreaStore["startoREQ_Preinstall"]; ?></li>
                    <li><?php echo $AreaStore["stratoREQ_VMhome"]; ?></li>
                    <li><?php echo $AreaStore["stratoREQ_UsageSSH"]; ?></li>
                </ul>
            </div>
            <!-- Setup Main Server -->
            <div style="display: block;">
                <div class="w3-twothird">
                    <h3><?php echo $AreaStore["InitialServerSetup"]; ?></h3>
                    <p>
<?php
$para = $AreaStore["RunInitialInstall"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$l1_str = str_replace(array("*", "#"), array("<a href=\"https://www.strato.de/apps/CustomerService\">", "</a>"), $nlstr);
$l2_str = str_replace(array("+", "~"), array("<a href=\"https://www.strato.de/apps/GoTo/serverlogin\">", "</a>"), $l1_str);
echo wordwrap($l2_str, 240, "\n", false) . "\n";
?>
                    </p>
                </div>
                <div class="w3-quarter w3-padding-small" style="float: right;">
                    <div style="margin: 20px;">
                        <p class="w3-tiny" id="C2Zoom"><?php echo $AreaStore["Click2Zoom"]; ?></p>
                    </div>
                    <div class="rtl-zoom">
                        <input type="checkbox" id="pictOS">
                        <label for="pictOS">
                            <img class="w3-mobile" src="/img/pveCloud/01_prep_VM/SL_InstallPage.png" style="width: 100%; max-width: 400px;">
                        </label>
                    </div>
                </div>
            </div>
            <!-- Installing a VM -->
            <div style="float: left;">
                <h3><?php echo $AreaStore["PVE_InstallVM"]; ?></h3>
                <p>
<?php ParaAreaStd($AreaStore["SetupVM"]); ?>
                </p>
                <!-- Prep VBOX NAT -->
                <div class="w3-padding w3-card-2" style="overflow: auto; margin: 5px;">
                    <h4><?php echo $AreaStore["PrepareVBOX"]; ?></h4>
                    <p>
<?php ParaAreaStd($AreaStore["VBOX_NAT_NW"]); ?>
                    </p>
                    <div class="mobile1000 mobile-Wmax370" style="float: right;">
                        <div class="smallTabStacked" style="float: left; margin: 5px;">
                            <img src="/img/pveCloud/01_prep_VM/vbox-nat-network-basic.png" style="width: 100%; max-width: 300px;">
                            <p class="w3-tiny">
                                <?php echo $AreaStore["PICT01"]; ?>
                            </p>
                        </div>
                        <div class="smallTabStacked" style="float: left; margin: 5px">
                            <img src="/img/pveCloud/01_prep_VM/vbox-nat-network-ports.png" style="width: 100%; max-width: 350px;" id="NatPorts">
                            <p class="w3-tiny">
                                <?php echo $AreaStore["PICT02"]; ?>
                            </p>
                        </div>
                    </div>
                    <p>
<?php
$para = $AreaStore["VBOX_Startup"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$b_str = str_replace(array("*", "#"), array("<b>", "</b>"), $nlstr);
echo wordwrap($b_str, 240, "\n", false) . "\n";
?>
                    </p>
                </div>
                <!-- Enter Setup -->
                <div class="w3-padding w3-card-2" style="overflow: auto; margin: 5px;">
                    <h4><?php echo $AreaStore["InstVM_CPset"]; ?></h4>
                    <p>
<?php
$para = $AreaStore["VM_Setup_ManyTasks"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$b_str = str_replace(array("*", "#"), array("<b>", "</b>"), $nlstr);
echo wordwrap($b_str, 240, "\n", false) . "\n";
?>
                    </p>
                    <!-- Disk and Partitioning -->
                    <div style="width: 100%; display: block;">
                        <div class="w3-twothird">
                            <h5><?php echo $AreaStore["VM_Setup_HDD"]; ?></h5>
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["VM_Setup_PartSize"]); ?>
                            </p>
                        </div>
                        <div class="w3-quarter w3-padding-small" style="float: right;">
                            <div class="rtl-zoom">
                                <input type="checkbox" id="pictSetupParts">
                                <label for="pictSetupParts">
                                    <img class="w3-mobile" src="/img/pveCloud/01_prep_VM/SetupDisk.png" style="width: 100%; max-width: 300px;">
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- Lanuage and Time Zone -->
                    <div style="width: 100%; display: block; overflow: auto;">
                        <h5><?php echo $AreaStore["VM_Setup_LangTZ"]; ?></h5>
                        <p class="w3-small">
<?php ParaAreaStd($AreaStore["VM_Setup_Region"]); ?>
                        </p>
                    </div>
                    <!-- Password and Mail for root -->
                    <div style="width: 100%; display: block;">
                        <div class="w3-twothird">
                            <h5><?php echo $AreaStore["VM_Setup_PassMail"]; ?></h5>
                            <p class="w3-small">
<?php ParaAreaStd($AreaStore["VM_Setup_PassMailp"]); ?>
                            </p>
                        </div>
                        <div class="w3-quarter w3-padding-small" style="float: right;">
                            <div class="rtl-zoom">
                                <input type="checkbox" id="pictSetupPassMail">
                                <label for="pictSetupPassMail">
                                    <img class="w3-mobile" src="/img/pveCloud/01_prep_VM/Setup_Password_Mail.png" style="width: 100%; max-width: 300px;">
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- Managemant Network Interface -->
                    <div style="width: 100%; display: block;">
                        <div class="w3-twothird">
                            <h5><?php echo $AreaStore["VM_Setup_Network"]; ?></h5>
                            <p class="w3-small">
<?php
$para = $AreaStore["VM_Setup_ManNetwork"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$b_str = str_replace(array("*", "#"), array("<samp class=\"w3-light-grey\">", "</samp>"), $nlstr);
echo wordwrap($b_str, 240, "\n", false) . "\n";
?>
                            </p>
                        </div>
                        <div class="w3-quarter w3-padding-small" style="float: right;">
                            <div class="rtl-zoom">
                                <input type="checkbox" id="pictSetupNetwork">
                                <label for="pictSetupNetwork">
                                    <img class="w3-mobile" src="/img/pveCloud/01_prep_VM/SetupNetwork.png" style="width: 100%; max-width: 300px;">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Let install and first reboot -->
                <div class="w3-padding w3-card-2" style="overflow: auto; margin: 5px;">
                    <h4><?php echo $AreaStore["Setup_Completition"]; ?></h4>
                    <p>
<?php
$para = $AreaStore["FinSetup_Reboot"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$l1_str = str_replace(array("*", "|"), array("<a href=\"#NatPorts\">", "</a>"), $nlstr);
$code_str = str_replace(array("^", "+"), array("<div style=\"background-color: black; color: white; width: max-content;\"><code>", "</code></div>"), $l1_str);
echo wordwrap($code_str, 240, "\n", false) . "\n";
?>
                    </p>
                    <img src="/img/pveCloud/01_prep_VM/pveFirstStart.png" style="width: 100%; max-width: 540px;">
                </div>
            </div>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>
