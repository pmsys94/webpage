<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 22;
include '../../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, -16); // Also include common pveCloud
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <style type="text/css">
            samp{
                font-size: 12px;
                line-height: 0;
                word-break: keep-all;
            }
            @media (min-width: 1201px) {
                .p-float-left {
                    width: calc(100% - 278px);
                    float: left;
                }
                .img-float-right {
                    float: right;
                }
            }
        </style>
    </head>
    <body>
<?php
include '../../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px; padding-right: 10px;">
            <h1><?php echo $AreaStore["Project_PVE_Cloud"]; ?></h1>
            <h2><?php echo $AreaStore["SerCons"]; ?></h2>
            <p>
<?php ParaAreaStd($AreaStore["SerCons4STRATO"]); ?>
            </p>
            <h3><?php echo $AreaStore["UseCase_SerCons"]; ?></h3>
            <p>
<?php ParaAreaStd($AreaStore["WhySerCons"]); ?>
            </p>
            <h3><?php echo $AreaStore["EnableSerCons"]; ?></h3>
            <p>
<?php ParaAreaStd($AreaStore["EnSerCons_Intro"]); ?>
            </p>
            <ul>
                <li>Baudrate: 57600 Baud</li>
                <li><?php echo $AreaStore["SER_Nettodata"]; ?>: 8</li>
                <li>Parity: <?php echo $AreaStore["SER_NO_Partiy"]; ?> (N)</li>
                <li>Stop Bits: 1</li>
                <li><?php echo $AreaStore["SER_DevEnum"]; ?>: 0 (Linux: ttyS0)</li>
            </ul>
            <h4><?php echo $AreaStore["SetConsGrub"]; ?></h4>
            <p>
<?php
$content = array("\"/etc/default/grub\"",
 "GRUB_CMDLINE_LINUX_DEFAULT=",
 "\"quiet splash\"",
 "console=tty0 console=ttyS0,57600",
 "GRUB_TERMINAL=console",
 "GRUB_SERIAL_COMMAND=\"serial --speed=57600 --unit=0 --word=8 --parity=no --stop=1\"",
 "update-grub");
$samp_keys = array();
for ($i=0; $i < count($content); $i++) {
    array_push($samp_keys, 'samp' . $i);
}
$para = $AreaStore["EnSerCons_Conf"];
$repl_str = str_replace($samp_keys, $content, $para);
$entity = htmlfix($repl_str);
$nlstr = nl2br($entity);
$samp_str = str_replace(array('*', '~'), array('<samp class="w3-light-grey">', '</samp>'), $nlstr);
echo wordwrap($samp_str, 240, "\n", false) . "\n";
?>
            </p>
            <h4><?php echo $AreaStore["DisableConsole"]; ?></h4>
            <p>
<?php ParaAreaStd($AreaStore["DisableConsoleGrub"]); ?>
            </p>
            <h3><?php echo $AreaStore["DebuggingVBOX"]; ?></h3>
            <div style="width: 99.5%; display: block; overflow: auto;">
                <p class="p-float-left">
<?php ParaAreaStd($AreaStore["DebuggingIntro"]); ?>
                </p>
                <img src="/img/pveCloud/04_tty/vbox-tty-test-settings.png" style="max-width: 275px; width: 100%;" class="img-float-right">
            </div>
            <h4><?php echo $AreaStore["TestVBOXCons"]; ?></h4>
            <p>
<?php
$para = $AreaStore["VBOX_Socat_Test"];
$cmd_str = "<samp class=\"w3-light-grey\">socat unix-connect:/tmp/ttyVBOX0 -,b57600</samp>";
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$samp_str = str_replace("#", $cmd_str, $nlstr);
echo wordwrap($samp_str, 240, "\n", false) . "\n";
?>
            </p>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>
