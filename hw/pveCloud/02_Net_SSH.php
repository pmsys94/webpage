<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 20;
include '../../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, 0); // Also include dummy page
$AreaStore += pullAreas($DBcon, $lang, -16); // Also include common pveCloud
function print_CMD_formated($output)
{
    $entity = htmlfix($output);
    $sp_str = str_replace(" ", "&nbsp;", $entity);
    $nl_str = nl2br($sp_str);
    echo $nl_str;
}
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <style type="text/css">
            code{
                font-size: 12px;
                line-height: 0;
            }
            .code-box {
                background-color: black;
                color: white;
                width: max-content;
            }
            .remsh-7 code{
                font-size: 7px;
            }
            .remsh-9 code {
                font-size: 9px;
            }
            @media (min-width: 600px) {
                .remsh code{
                    font-size: 12px !important;
                }
                b.bash-br {
                    display: none;
                }
            }
            @media (min-width: 1200px) {
                p.shell-share-space {
                    float: left;
                    margin-right: 5px;
                }
                div.shell-share-space {
                    float: right;
                    margin-left: 5px;
                }
                div.flex-share {
                    display: flex;
                    align-items: baseline;
                }
                div.flex-share p {
                    margin-right: 10px;
                }
                div.flex-shell {
                    flex-shrink: 0;
                }
            }
        </style>
    </head>
    <body>
<?php
include '../../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px; padding-right: 10px;">
            <h1><?php echo $AreaStore["Project_PVE_Cloud"]; ?></h1>
            <h2><?php echo $AreaStore["Net_and_SSH"]; ?></h2>
            <p>
<?php ParaAreaStd($AreaStore["IntoNetSSH"]); ?>
            </p>
            <!-- actual STRATO Server network settings -->
            <h3><?php echo $AreaStore["ActualNetworkSTRATO"]; ?></h3>
            <p class="shell-share-space">
<?php ParaAreaStd($AreaStore["GetNetworkActual"]); ?>
            </p>
            <div class="shell-share-space">
                <p style="font-style: italic;"><?php echo $AreaStore["CMD_line_is"]; ?></p>
                <div class="code-box">
                    <code style="white-space: break-spaces;">cat /etc/resolv.conf &amp;&amp; ip route &amp;&amp; <b class="bash-br">&bsol;<br></b>ip a &amp;&amp; cat /etc/hosts &amp;&amp; cat /etc/hostname</code>
                </div>
                <p style="font-style: italic;"><?php echo $AreaStore["CMD_Output"] . " " . $AreaStore["DataChangedDemo"]; ?></p>
                <div class="code-box remsh remsh-7">
                    <code>
<?php
$cmd_output = "domain stratoserver.net
search stratoserver.net
nameserver 81.169.163.106
nameserver 85.214.7.22
nameserver 81.169.148.34
default via 85.214.254.42 dev eno1 
85.214.254.42 dev eno1 scope link 
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eno2: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc pfifo_fast state DOWN group default qlen 1000
    link/ether 00:ab:cd:ef:01:22 brd ff:ff:ff:ff:ff:ff
    altname enp0s25
3: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:ab:cd:ef:01:01 brd ff:ff:ff:ff:ff:ff
    altname enp3s0
    inet 85.214.5.42/32 brd 85.214.5.42 scope global dynamic eno1
       valid_lft 66927sec preferred_lft 66927sec
127.0.0.1   localhost
127.0.1.1   h0000000.stratoserver.net   h0000000

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
h0000000.stratoserver.net";
print_CMD_formated($cmd_output);
?>
                    </code>
                </div>
            </div>
            <!-- Network interface config on pve -->
            <h3><?php echo $AreaStore["PVE_NetworkIF"]; ?></h3>
            <p>
<?php ParaAreaStd($AreaStore["PVE_NW_Overview"]); ?>
            </p>
            <h4><?php echo $AreaStore["PVE_Net1_bridged"]; ?></h4>
            <p>
<?php ParaAreaStd($AreaStore["BridgedNetworkMode"]); ?>
            </p>
            <h4><?php echo $AreaStore["PVE_Net2_NAT"]; ?></h4>
            <p>
<?php ParaAreaStd($AreaStore["NattedNetworking"]); ?>
            </p>
            <h4><?php echo $AreaStore["PVE_NW_CloudConf"]; ?></h4>
            <div class="flex-share">
                <div>
                    <p>
<?php ParaAreaStd($AreaStore["ChangeNWConf"]); ?>
                    </p>
                    <p><?php echo $AreaStore["MoreNetOptInfo"] . " <a href=\"https://pve.proxmox.com/wiki/Network_Configuration\">" . $AreaStore["SeeMore"] . "</a>"; ?></p>
                </div>
                <div class="code-box remsh remsh-9 flex-shell">
                    <code>
<?php
$cmd_output = "root@h0000000:~# cat /etc/network/interfaces
auto lo
iface lo inet loopback

auto enp0s3
iface enp0s3 inet static
    address 192.168.55.4/24
    gateway 192.168.55.1

auto vmbr0
iface vmbr0 inet static
    address 192.168.9.1/24
    bridge-ports none
    bridge-stp off
    bridge-fd 0
    post-up   echo 1 > /proc/sys/net/ipv4/ip_forward
    post-up   iptables -t nat -A POSTROUTING -s '192.168.9.1/24' -o enp0s3 -j MASQUERADE
    post-down iptables -t nat -D POSTROUTING -s '192.168.9.1/24' -o enp0s3 -j MASQUERADE";
print_CMD_formated($cmd_output);
?>
                    </code>
                </div>
            </div>
            <h3><?php echo $AreaStore["ChangingSSH"]; ?></h3>
            <p>
<?php ParaAreaStd($AreaStore["WhyChangeSSH"]); ?>
            </p>
            <h4><?php echo $AreaStore["ChangeSSHD"]; ?></h4>
            <p>
<?php ParaAreaStd($AreaStore["ChangeSSH_p"]); ?>
            </p>
            <div class="code-box" id="sshd_diff">
                <code>
<?php
$cmd_output = "diff /etc/ssh/sshd_config /etc/ssh/sshd_config.new
15c15
< #Port 22
---
> Port 2222
34c34
< PermitRootLogin yes
---
> PermitRootLogin prohibit-password
39c39
< #PubkeyAuthentication yes
---
> PubkeyAuthentication yes
58c58
< #PasswordAuthentication yes
---
> PasswordAuthentication no";
print_CMD_formated($cmd_output);
?>
                </code>
            </div>
            <h4><?php echo $AreaStore["SSH_Pubkey_Config"]; ?></h4>
            <p>
<?php
$para = $AreaStore["SSH_Keygen_Config"];
$entity = htmlfix($para);
$nl_str = nl2br($entity);
$repl_str = str_replace(array("*", "#"), array("<samp class=\"code-box\">", "</samp>"), $nl_str);
echo wordwrap($repl_str, 240, "\n", false) . "\n";
?>
            </p>
            <h4><?php echo $AreaStore["SSH_CL_Conf"]; ?></h4>
<?php
echo "            <p>\n";
ParaAreaStd($AreaStore["SSH_CL_AddConf"]);
echo "            </p>\n";
echo "            <div class=\"code-box\">\n";
echo "              <code>\n";
$cmd_output = "Host stratoserver
    Hostname h0000000.stratoserver.net
    User root
    Port 2222
    LocalForward 8006 127.0.0.1:8006";
print_CMD_formated($cmd_output);
echo "              </code>\n";
echo "            </div>\n";
?>
            <p>
<?php ParaAreaStd($AreaStore["SSH_ApplyConf"]); ?>
            </p>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>
