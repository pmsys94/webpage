<!DOCTYPE HTML>
<html>
      <head>
<?php
$pid = 3;
include '../php/std-head.inc';
?>
        <title><?php echo $page["HTML_Title"]; ?></title>
        <link rel="icon" href="/pm_fav.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <style type="text/css">
        	#picME{
        		width: 170px;
        		height: auto;
        		float: left;
        	}
          .w3-card-4{
            background-color: white;
          }
        </style>
      </head>
      <body>
<?php include '../php/std-menu-head.inc'; echo "\n";?>
          <div id="Inhalt" class="w3-container">
            <!-- Card me -->
                <div class="w3-card-4">
                  <!-- Pict DIV -->
                  <h3 class="w3-teal"><?php echo $AreaStore["headMe"]; ?></h3>
                  <div>
                	 <img id="picME" src="/img/Me.png">
                  </div>
                  <!-- TXT DIV -->
                  <div>
                  	<p>
<?php ParaAreaStd($AreaStore["paraMe"]); ?>
                  	</p>
                    <p>
<?php ParaAreaStd($AreaStore["paraMeMainly"]); ?>
                    </p>
                  </div>
                  <!-- END TXT DIV -->
                </div>
                <!-- End card me -->
                <div class="w3-card-4">
                  <h3 class="w3-teal"><?php echo $AreaStore["headAdmin"]; ?></h3>
                  <p>
<?php ParaAreaStd($AreaStore["paraAdmin"]); ?>                    
                  </p>
                </div>
                <div class="w3-card-4">
                  <h3 class="w3-teal"><?php echo $AreaStore["head_OpSys"]; ?></h3>
                  <p>
<?php ParaAreaStd($AreaStore["paraOpSys"]); ?>                    
                  </p>
                </div>
                <div class="w3-card-4">
                  <h3 class="w3-teal"><?php echo $AreaStore["headSoftDev"]; ?></h3>
                  <p>
<?php
$txt = $AreaStore["paraSoftDev"];
$txt2 = $AreaStore["paraSoftDev2"];
$txt3 = $AreaStore["paraSoftDev3"];
$txtEnt = htmlfix($txt);
$txtEnt2 = htmlfix($txt2);
$txtEnt3 = htmlfix($txt3);
echo nl2br($txtEnt) . "\n";
?>                    
                  </p>
                  <p>
<?php echo nl2br($txtEnt2); ?>
                  </p>
                  <p>
<?php echo nl2br($txtEnt3); ?>
                  </p>
                </div>
          </div>
      </body>
</html><?php mysqli_close($DBcon); ?>