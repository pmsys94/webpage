			<div id="MOP-hd">
				<p>
					<?php ParaAreaStd($AreaStore["MOP_Hide"]); ?>
				</p>
				<img src="/img/study/se2-asm-diceEyes.png">
			</div>
			<div id="syscall-hd">
				<p>
					<?php ParaAreaStd($AreaStore["syscallHidden"]); ?>
				</p>
				<img src="/img/study/se4-rts-pmtest.png">
				<img src="/img/study/se4-rts-ptqsum.png">
			</div>
			<div id="ios-hd">
				<p>
					<?php ParaAreaStd($AreaStore["iosHiddenPara"]); ?>
				</p>
				<img src="/img/study/wp-ios-wpt-berlin.png">
				<img src="/img/study/wp-ios-wpt-tegel.png">
			</div>
			<div id="tcp-hd">
				<p>
					<?php ParaAreaStd($AreaStore["VsysHiddenInfo"]); ?>
				</p>
				<img src="/img/study/se4-vsys-tcps-term.png">
				<img src="/img/study/se4-vsys-tcps-code.png">
			</div>
			<div id="its-hd">
				<p>
					<?php ParaAreaStd($AreaStore["itsHiddenPara"]); ?>
				</p>
				<img src="/img/study/wp-its-subst-term.png">
				<img src="/img/study/wp-its-cbc-diff.png">
			</div>
			<div id="dbs-hd">
				<p>
					<?php ParaAreaStd($AreaStore["DBS-ModalText"]); ?>
				</p>
				<img src="/img/study/se4-dbs-outpH.png">
				<img src="/img/study/se4-dbs-outWS.png">
				<img src="/img/study/nintendo_OtherConsole.png" id="IMG_DBS_PieChart"><!-- Not Captured by zview3 using DIVs id, but set to it's Function over the first Image Parameter using getElementsById() ! -->
			</div>