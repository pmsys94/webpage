<!DOCTYPE HTML>
<html>
      <head>
<?php
$pid = 4;
include '../../php/std-head.inc';
?>
        <title><?php echo $page["HTML_Title"]; ?></title>
        <link rel="icon" href="/pm_fav.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/js/zview3/zview3-style.css">
        <script type="text/javascript" src="/js/zview3/zview3.js"></script>
        <style type="text/css">
          .w3-card-4{
            background-color: white;
          }
          img{
            width: 100%;
            height: auto;
          }
        </style>
      </head>
      <body onload="zview3_init()">
<?php include '../../php/std-menu-head.inc'; echo "\n";?>
          <div id="Inhalt" class="w3-container">
            <h4><?php echo $AreaStore["PageHead"]; ?></h4>
            <p><?php echo htmlfix( $AreaStore["Headliner"] ); ?></p>
            <!-- Group 1 -->
            <div class="w3-third">
              <!-- SysProg & Echtzeit-->
              <div class="w3-card-4">
                <img class="zv3-img" src="/img/study/se3-sysprog-pswsh.png" onclick="zv3_i1c(this, 'syscall-hd', 3)">
                <h4 class="w3-teal"><?php echo $AreaStore["syscallH4"]; ?></h4>
                <p>
<?php ParaAreaStd($AreaStore["unixCodeMini"]); ?>
                </p>                
              </div>
              <!-- Vsys -->
              <div class="w3-card-4">
                <img class="zv3-img" style="max-width: 540px;" src="/img/study/se4-vsys-tcps-ff.png" onclick="zv3_i1c(this, 'tcp-hd', 3)">
                <h4 class="w3-teal"><?php echo $AreaStore["IPswH4"]; ?></h4>
                <p>
<?php ParaAreaStd($AreaStore["VsysMiniInfo"]); ?>
                </p>                
              </div>
            </div>
            <!-- Group 2 -->
            <div class="w3-third">
              <!-- MOP -->
              <div class="w3-card-4">
                <img class="zv3-img" style="max-width: 540px;" src="/img/study/edsim.png" onclick="zv3_i1c(this, 'MOP-hd', 2)">
               <h4 class="w3-teal"><?php echo $AreaStore["MOP_HEAD"]; ?></h4>
                <p>
<?php ParaAreaStd($AreaStore["MOP_Text"]); ?>
                </p>
              </div>
              <!-- DB Sys -->
              <div class="w3-card-4">
                <img class="zv3-img" style="max-width: 540px;" src="/img/study/se4-dbs-schema.png" onclick="zv3_i1c(document.getElementById('IMG_DBS_PieChart'), 'dbs-hd', 3)">
                <h4 class="w3-teal"><?php echo $AreaStore["DBSYS_HEAD"]; ?></h4>
                <p>
<?php ParaAreaStd($AreaStore["DBS-miniPara"]); ?>
                </p>                
              </div>
            </div>
            <!-- Group 3 -->
            <div class="w3-third">
              <!-- iOS -->
              <div class="w3-card-4">
                <img class="zv3-img" style="max-width: 540px;" src="/img/study/wp-ios-wpt-xcode.png" onclick="zv3_i1c(this, 'ios-hd', 3)">
                <h4 class="w3-teal"><?php echo $AreaStore["iosH4"]; ?></h4>
                <p>
<?php ParaAreaStd($AreaStore["iOS_MiniPara"]); ?>
                </p>                
              </div>
              <!-- ITS -->
              <div class="w3-card-4">
                <img class="zv3-img" src="/img/study/wp-its-SecC.png" onclick="zv3_i1c(this, 'its-hd', 3)">
                <h4 class="w3-teal"><?php echo $AreaStore["itsH4"]; ?></h4>
                <p>
<?php ParaAreaStd($AreaStore["itsMiniPara"]); ?>
                </p>               
              </div>
            </div>
            <!-- End Group 3 -->

          </div>
          <!-- Modal -->
        <div id="zview3" class="zv3-modal">
            <span class="zv3-close" onclick="zv3_sCc()">&times;</span>
            <div id="zv3-array">
                <img class="modal-image" id="modimg1">
                <img class="modal-image" id="modimg2" style="display: none;">
                <img class="modal-image" id="modimg3" style="display: none;">
            </div>
            <div id="modpara"></div>
        </div>
          <!-- Hidden Objects -->
          <div class="w3-hide">
<?php
include 'hidden-paras.inc';
?>              
          </div>
      </body>
</html><?php mysqli_close($DBcon); ?>