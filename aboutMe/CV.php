<!DOCTYPE html>
<html>
	<head>
<?php
$pid = 2;
include '../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, 0); // Also include dummy page areas
$monthCSV = $AreaStore["MonthsShort"];
$months = explode(",", $monthCSV);
?>
		<title><?php echo htmlfix($page["HTML_Title"]); ?></title>
		<link rel="icon" href="/pm_fav.ico">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/styles/w3.css">
		<link rel='stylesheet' href='/mirror/Roboto.css'>
		<link rel="stylesheet" href="/mirror/css/font-awesome.min.css">
		<link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
		<style>
		html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
		</style>
	</head>
	<body class="w3-light-grey">
	<!-- Page Container -->
	<div>
<?php include '../php/std-menu-head.inc'; echo "\n";?>
	  <!-- The Grid -->
	  <div class="w3-row-padding">
	  
	    <!-- Left Column -->
	    <div class="w3-third" style="max-width:461px;">
	    
	      <div class="w3-white w3-text-grey w3-card-4">
	        <div class="w3-display-container">
	          <img src="/img/Me.png" style="width:100%" alt="Avatar">
	          <div class="w3-display-bottomleft w3-container w3-text-black">
	            <h2 style="color: white;">Pierre Mei&szlig;ner</h2>
	          </div>
	        </div>
	        <div class="w3-container">
	          <p><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>Software Developer&ensp;<a href="https://gitlab.com/pmsys94"><i class="fa fa-gitlab"></i></a></p>
	          <p><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>Berlin, DE</p>
	          <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i><a href="mailto:cv@pm-sys.ddns.net">cv@pm-sys.ddns.net</a></p>
	          <p><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i><a href="tel:+493012088421">+49 30 12088421</a></p>
	          <hr>

	          <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i>Skills</b></p>
	          <p>Software Development</p>
	          <div class="w3-light-grey w3-round-xlarge">
	            <div class="w3-round-xlarge w3-teal" style="height:24px;width:78%"></div>
	          </div>
	          <p><?php echo $AreaStore["BarVirt"]; ?></p>
	          <div class="w3-light-grey w3-round-xlarge">
	            <div class="w3-round-xlarge w3-teal" style="height:24px;width:75%"></div>
	          </div>
	          <p>Administration</p>
	          <div class="w3-light-grey w3-round-xlarge">
	            <div class="w3-round-xlarge w3-teal" style="height:24px;width:65%"></div>
	          </div>

	          <p class="w3-large w3-text-theme"><b><i class="fa fa-globe fa-fw w3-margin-right w3-text-teal"></i><?php echo $AreaStore["headLangSkills"]; ?></b></p>
	          <p><?php echo $AreaStore["langDE"]; ?></p>
	          <div class="w3-light-grey w3-round-xlarge">
	            <div class="w3-round-xlarge w3-teal" style="height:24px;width:100%"></div>
	          </div>
	          <p><?php echo $AreaStore["langEN"]; ?></p>
	          <div class="w3-light-grey w3-round-xlarge">
	            <div class="w3-round-xlarge w3-teal" style="height:24px;width:55%"></div>
	          </div>
	          <p><?php echo $AreaStore["LangIT"]; ?></p>
	          <div class="w3-light-grey w3-round-xlarge">
	            <div class="w3-round-xlarge w3-teal" style="height:24px;width:10%"></div>
	          </div>
	          <br>
	        </div>
	      </div><br>

	    <!-- End Left Column -->
	    </div>

	    <!-- Right Column -->
	    <div class="w3-twothird">
	    <!-- IT Skills Container-->
	    	<div class="w3-container w3-card w3-white w3-margin-bottom">
	        	<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i>IT Skills</h2>
	        	<!-- Left - Software Skills-->
	          <div class="w3-half w3-margin-bottom">
	          		<p class="w3-large w3-text-theme"><i class="fa fa-code fa-fw w3-margin-right w3-text-teal"></i>Software</p>
		          <p><?php $cprog=$AreaStore["cprog"]; echo $cprog; ?> (Gen.)</p>
		          <div class="w3-light-grey w3-round-xlarge w3-small">
		            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:80%">80%</div>
		          </div>
		          <p><?php echo $cprog; ?> (Linux low-level; Embedded Sys.)</p>
		          <div class="w3-light-grey w3-round-xlarge w3-small">
		            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:40%">
		              <div class="w3-center w3-text-white">40%</div>
		            </div>
		          </div>
		          <p>PHP</p>
		          <div class="w3-light-grey w3-round-xlarge w3-small">
		            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:30%">30%</div>
		          </div>
		          <p><?php echo $AreaStore["OtherOO"]; ?> (VB.NET; C++; Swift; Python)</p>
		          <div class="w3-light-grey w3-round-xlarge w3-small">
		            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:40%">40%</div>
		          </div>
	          </div>
	          <!-- Right - Hardware Skills-->
	          <div class="w3-half w3-margin-bottom">
		          <p class="w3-large w3-text-theme"><i class="fa fa-server fa-fw w3-margin-right w3-text-teal" aria-hidden="true"></i>Hardware</p>
		          <p><?php echo $AreaStore["networks"]; ?></p>
		          <div class="w3-light-grey w3-round-xlarge w3-small">
		            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:60%">60%</div>
		          </div>
		          <p>Raspberry PI</p>
		          <div class="w3-light-grey w3-round-xlarge w3-small">
		            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:45%">45%</div>
		          </div>
		          <p>Arduino</p>
		          <div class="w3-light-grey w3-round-xlarge w3-small">
		            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:35%">35%</div>
		          </div>
		          <p>FPGA Hardware Design</p>
		          <div class="w3-light-grey w3-round-xlarge w3-small">
		            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:20%">
		            <div class="w3-center w3-text-white">20%</div>
		        	</div>
		          </div>
		      </div>

		      <!-- OS Skills-->
		      <div style="width: 100%; float: left;">
		      	<p class="w3-large w3-text-theme"><i class="fa fa-terminal fa-fw w3-margin-right w3-text-teal" aria-hidden="true"></i><?php echo $AreaStore["SkillsOS"]; ?></p>
		      	<!-- Desktop OS-->
		      	<div style="width: 100%; float: left;">
		      		<p class="w3-large w3-text-theme"><i class="fa fa-desktop fa-fw w3-margin-right w3-text-teal" aria-hidden="true"></i>Desktop OS</p>
			      	<div class="w3-third">
			      		<p><i class="fa fa-windows"></i>Windows</p>
			          <div class="w3-light-grey w3-round-xlarge w3-small">
			            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:80%">80%</div>
			          </div>
			      	</div>
			      	<div class="w3-third">
			      		<p><i class="fa fa-linux"></i>(Debian) Linux</p>
			          <div class="w3-light-grey w3-round-xlarge w3-small">
			            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:65%">65%</div>
			          </div>
			      	</div>
			      	<div class="w3-third">
			      		<p><i class="fa fa-apple"></i>Mac OS</p>
			          <div class="w3-light-grey w3-round-xlarge w3-small">
			            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:45%">45%</div>
			          </div>
			      	</div>
		      	</div>
		      </div>
		      <!-- Mobile OS-->
		      <div class="w3-margin-bottom" style="width: 100%; float: left;">
		      	<p class="w3-large w3-text-theme"><i class="fa fa-tablet fa-fw w3-margin-right w3-text-teal" aria-hidden="true"></i>Mobile OS</p>
			      	<div class="w3-half">
			      		<p><i class="fa fa-android"></i>Andoid</p>
			          <div class="w3-light-grey w3-round-xlarge w3-small">
			            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:70%">70%</div>
			          </div>
			      	</div>
			      	<div class="w3-half">
			      		<p><i class="fa fa-apple"></i>iOS</p>
			          <div class="w3-light-grey w3-round-xlarge w3-small">
			            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:35%">35%</div>
			          </div>
			      	</div>
		      </div>
	      </div>
	      <!-- Work Container-->
	      <div class="w3-container w3-card w3-white w3-margin-bottom">
	        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-industry fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i><?php echo $AreaStore["workH2"]; ?></h2>
	        <!-- Work at strato -->
	        <div class="w3-container">
	          <h5 class="w3-opacity"><b><a href="https://www.strato.de/"><?php echo htmlfix( $AreaStore["workStrato"] ); ?></a></b></h5>
	          <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>
	          	<?php echo $months[3-1] . " 2023 - "; ?>
	          	<span class="w3-tag w3-teal w3-round"><?php echo $AreaStore["workUpNow"]; ?></span>
	          </h6>
	          <p>
<?php ParaAreaStd($AreaStore["workStrato_p"]); ?>
	          </p>
	          <hr>
	        </div>
	        <!-- Work at AVM -->
	        <div class="w3-container">
	          <h5 class="w3-opacity"><b><a href="https://avm.de/"><?php echo htmlfix( $AreaStore["workAVM"] ); ?></a></b></h5>
	          <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>
	          	<?php echo $months[9-1] . " 2022 - " . $months[2-1] . " 2023\n"; ?>
	          </h6>
	          <p>
<?php ParaAreaStd($AreaStore["workAVM_P"]); ?>
	          </p>
	          <hr>
	        </div>
	        <!-- Work at Otis -->
	        <div class="w3-container">
	          <h5 class="w3-opacity"><b><a href="https://www.otis.com/de/de/"><?php echo htmlfix( $AreaStore["workOtis"] ); ?></a></b></h5>
	          <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>
	          	<?php echo $months[10-1] . " 2019 - " . $months[9-1] . " 2021\n"; ?>
	          </h6>
	          <p>
<?php ParaAreaStd($AreaStore["workOtis_P"]); ?>
	          </p>
	          <hr>
	        </div>
	        <!-- Work at Data CIB -->
	        <div class="w3-container">
	          <h5 class="w3-opacity"><b><a href="http://datacib.de/"><?php echo htmlfix( $AreaStore["workDCIB"] ); ?></a></b></h5>
	          <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>
	          <?php echo $months[10-1] . " 2017 - " . $months[9-1] . " 2019\n"; ?>
	      		</h6>
	          <p>
<?php ParaAreaStd($AreaStore["workDCIB_P"]); ?>
	          </p>
	        </div>
	      </div>
	      <!-- Edu Container -->
	      <div class="w3-container w3-card w3-white">
	        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-certificate fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i><?php echo $AreaStore["schoolH2"]; ?></h2>
	        <div class="w3-container">
	        	<!-- BHT -->
	          <h5 class="w3-opacity"><i class="fa fa-university"></i><b><a href="https://www.bht-berlin.de/b-ti"><?php echo htmlfix( $AreaStore["beuth"] ); ?></a></b></h5>
	          <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>2016 - 2022</h6>
	          <p><?php echo $AreaStore["schoolBeachlor"]; ?></p>
	          <p><?php ParaAreaStd($AreaStore["bht_content"]); echo "<a href=\"/aboutMe/study" . $link_lang . "\">" . $AreaStore["SeeMore"]; ?></a></p>
	          <h5><?php echo $AreaStore["Head_BA"]; ?></h5>
	          <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>
	          <?php echo $months[12-1] . " 2021 - " . $months[4-1] . " 2022 " . $AreaStore["Planned"] . " WS21/22"; ?>
	      		</h6>
	      		<p><?php ParaAreaStd($AreaStore["BA_About"]); ?></p>
	          <hr>
	        </div>
	        <!-- TA OSZ -->
	        <div class="w3-container">
	          <h5 class="w3-opacity"><i class="fa fa-briefcase"></i><b><?php $bfs = $AreaStore["schoolBFS"]; echo $bfs; ?></b></h5>
	          <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>2013 - 2016</h6>
	          <p>
<?php ParaAreaStd($AreaStore["schoolTA"]); ?>
	      		</p>
	          <hr>
	        </div>
	        <!-- SkE OSZ -->
	        <div class="w3-container">
	          <h5 class="w3-opacity"><i class="fa fa-briefcase"></i><b><?php echo $bfs; ?></b></h5>
	          <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>2011 - 2013</h6>
	          <p>
<?php ParaAreaStd($AreaStore["schoolSkE"]); ?>
	      		</p>
	          <hr>
	        </div>
	        <!-- Allgemeine Schule -->
	        <div class="w3-container" style="width: 100%; float: left;">
	        	<h4 class="w3-opacity"><i class="fa fa-graduation-cap"></i><?php echo $AreaStore["schoolH4"]; ?></h4>
	        	<div class="w3-third" style="float: left;">
	        		<h5 class="w3-opacity"><b><?php echo $bfs; ?></b></h5>
	          		<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>2013 - 2016</h6>
	          		<p><?php echo $AreaStore["schoolAbi"]; ?></p>
	        	</div>
	        	<div class="w3-third">
	        		<h5 class="w3-opacity"><b><?php echo $bfs; ?></b></h5>
	          		<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>2011 - 2013</h6>
	          		<p><?php echo $AreaStore["schoolMSA"]; ?></p>
	        	</div>
	        	<div class="w3-third" style="float: right;">
	        		<h5 class="w3-opacity"><b><?php echo $AreaStore["schoolMain"] ?></b></h5>
	          		<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>2001 - 2011</h6>
	          		<p><?php echo $AreaStore["schoolHaupt"]; ?></p>
	        	</div>
	          <br>
	        </div>
	      </div>

	    <!-- End Right Column -->
	    </div>
	    
	  <!-- End Grid -->
	  </div>
	  
	  <!-- End Page Container -->
	</div>

	<footer class="w3-container w3-teal w3-center w3-margin-top">
		
	  <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a> and <a href="http://www.html-seminar.de/toggle-menue-ohne-javascript.htm">HTML-Seminar.de</a></p>
	</footer>

	</body>
</html>
<?php mysqli_close($DBcon); ?>
