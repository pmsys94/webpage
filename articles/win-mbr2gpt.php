<!DOCTYPE HTML>
<html>
	<head>
<?php
$pid = 18;
include '../php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, -19); // Also include dummy page for common aritcle items
?>
		<link rel="icon" href="/pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="/styles/menu_and_main.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/styles/sidebar.css">
        <link rel="stylesheet" type="text/css" href="/styles/article-tags.css">
        <link rel="stylesheet" type="text/css" href="/js/pict-modal/pictmodal-style.css">
		<script type="text/javascript" src="/js/pict-modal/pictmodal.js"></script>
        <style type="text/css">
        	code{
                font-size: 12px;
                line-height: 0;
            }
			ol.pict-lane li{
				float: left;
				margin-left: 15px;
				margin-right: 15px;
				list-style-position: inside;
				max-width: 200px;
				word-break: break-word;
			}
			ol.pict-lane-no-numbers li{
				list-style: none;
			}
			ol.pict-lane img {
				max-width: 200px;
			}
			img.flright {
				max-width: 260px;
				float: right;
			}
			div.para-code-share{
				width: 100%;
				display: block;
				float: left;
			}
			p.para-code-share{
				float: left;
			}
			@media (min-width: 1200px){
				p.para-code-share{
					width: 74.99999%;
				}
				div.para-code-share{
					width: 24.99999%;
				}
			}
        </style>
    </head>
    <body onload="pictmodal_init()">
<?php
include '../php/std-menu-head.inc';
echo "\n";
include 'subitems.inc';
include '../php/std-menu-sbar.inc';
echo "\n";
?>
		<div id="Inhalt" style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
			<h1><?php echo $AreaStore["MBR2GPT_Header"]; ?></h1>
			<!-- Tags for this article -->
			<div style="float: left; margin-bottom: 10px; width: 100%;">
				<ul class="article-tag">
					<li style="border-top: solid red; border-right: solid green; border-bottom: solid yellow; border-left: solid blue; background-color: white; color: black;">Windows</li>
					<li>Windows 10 >= 1703</li>
					<li>Windows 11</li>
					<li style="background-color: black; color: white; border: solid black;"><?php echo $AreaStore["TAG_CommandLine"]; ?></li>
					<li style="border: black solid;"><?php echo $AreaStore["TAG_Partitions"]; ?></li>
					<li style="background-color: red; color: white; border: solid red;"><?php echo $AreaStore["TAG_RISKY"]; ?></li>
				</ul>
			</div>
			<!-- Intro into article -->
			<p class="w3-small">
<?php 
$para = $AreaStore["WhatMBR2GPT"];
$entity = htmlfix($para);
$nlstr = nl2br($entity) . "\n";
$repl =  str_replace(array("*", "#"), array("<a href=\"https://learn.microsoft.com/de-de/windows/deployment/mbr-to-gpt\">", "</a>"), $nlstr);
echo wordwrap($repl, 240, "\n", false) . "\n";
?>
			</p>
			<!-- Background discussion -->
			<div class="w3-card-4 w3-padding" style="background-color: white; overflow: auto; width: 98%;">
				<h2><?php echo $AreaStore["WhatTopic"]; ?></h2>
				<p class="w3-small">
<?php ParaAreaStd($AreaStore["TopicBackgr"]); ?>
				</p>
				<h3><?php echo $AreaStore["PartTypeHist"]; ?></h3>
				<p class="w3-small">
<?php ParaAreaStd($AreaStore["PartSchemaEFI"]); ?>
				</p>
				<h3><?php echo $AreaStore["ChallengeMultiResc"]; ?></h3>
				<p class="w3-small">
<?php ParaAreaStd($AreaStore["HavingMutipleResc"]); ?>
				</p>
			</div>
			<!-- Preconditions -->
			<div class="w3-card-4 w3-padding w3-light-green" style="overflow: auto; width: 98%; margin-top: 5px;">
				<h2><?php echo $AreaStore["Preconditions"]; ?></h2>
				<ul>
					<li><?php echo $AreaStore["BackupDisk"]; ?></li>
					<li><?php echo $AreaStore["NoExtendedParts"]; ?></li>
					<li><?php echo $AreaStore["MaxPrimParts"]; ?></li>
					<li><?php echo $AreaStore["FreeSpaceingStartEnd"]; ?></li>
					<li><a href="https://securedl.cdn.chip.de/downloads/18634680/Bootice1332.zip">Bootice (Chip.de)</a> <?php echo $AreaStore["LinkBootice"]; ?></li>
					<li><?php echo $AreaStore["LinuxLiveImage"]; ?></li>
					<li><?php echo $AreaStore["WinISO"]; ?></li>
				</ul>
			</div>
			<!-- BCD fix One - Remove the recovery -->
			<div class="w3-card-4 w3-padding" style="background-color: white; overflow: auto; width: 98%; margin-top: 5px;">
				<h2><?php echo $AreaStore["BCD_DupReco"]; ?></h2>
				<p class="w3-small">
<?php ParaAreaStd($AreaStore["DelDupRecoBCD"]); ?>
				</p>
				<p class="w3-small" style="font-weight: bold;">
<?php ParaAreaStd($AreaStore["HintReco2Step"]); ?>
				</p>
				<h3><?php echo $AreaStore["Bootice4DupReco"]; ?></h3>
				<p class="w3-small w3-threequarter">
<?php ParaAreaStd($AreaStore["WinRebootBootice"]); ?>
				</p>
				<img src="/img/articles/mbr2gpt/unziped-bootice.png" class="w3-quarter">
				<div style="width: 100%; display: block; overflow: auto;">
					<ol class="pict-lane">
						<li>
							<?php
							echo "<img class=\"w3-hover-opacity\" src=\"/img/articles/mbr2gpt/winsetup-lang.png\" alt=\"" . htmlfix($AreaStore["WinSetup_ACK_Lang"]) . "\" onclick=\"imgClick(this)\"><br>\n";
							echo "							" . htmlfix($AreaStore["WinSetup_ACK_Lang"]);
							?>
						</li>
						<li>
							<?php
							echo "<img class=\"w3-hover-opacity\" src=\"/img/articles/mbr2gpt/winsetup_repair.png\" alt=\"" . htmlfix($AreaStore["WinSetup_RepairClick"]) . "\" onclick=\"imgClick(this)\"><br>\n";
							echo "							" . htmlfix($AreaStore["WinSetup_RepairClick"]);
							?>
						</li>
						<li>
							<?php
							echo "<img class=\"w3-hover-opacity\" src=\"/img/articles/mbr2gpt/winsetup-problems.png\" alt=\"" . htmlfix($AreaStore["WinSetup_ExtRepOpts"]) . "\" onclick=\"imgClick(this)\"><br>\n";
							echo "							" . htmlfix($AreaStore["WinSetup_ExtRepOpts"]);
							?>
						</li>
						<li>
							<?php
							echo "<img class=\"w3-hover-opacity\" src=\"/img/articles/mbr2gpt/winsetup_cmd.png\" alt=\"" . htmlfix($AreaStore["WinSetup_CMD"]) . "\" onclick=\"imgClick(this)\"><br>\n";
							echo "							" . htmlfix($AreaStore["WinSetup_CMD"]);
							?>
						</li>
						<li>
							<?php
							echo "<img class=\"w3-hover-opacity\" src=\"/img/articles/mbr2gpt/winsetup-notepad.png\" alt=\"" . htmlfix($AreaStore["WinSetup_notepad"]) . "\" onclick=\"imgClick(this)\"><br>\n";
							echo "							" . htmlfix($AreaStore["WinSetup_notepad"]);
							?>
						</li>
						<li>
							<?php
							echo "<img class=\"w3-hover-opacity\" src=\"/img/articles/mbr2gpt/winsetup-n-sel-bootice.png\" alt=\"" . htmlfix($AreaStore["WinSetup_N_Open"]) . "\" onclick=\"imgClick(this)\"><br>\n";
							echo "							" . htmlfix($AreaStore["WinSetup_N_Open"]);
							?>
						</li>
						<li>
							<?php
							echo "<img class=\"w3-hover-opacity\" src=\"/img/articles/mbr2gpt/winsetup_n_open_bootice.png\" alt=\"" . htmlfix($AreaStore["WinSetup_N_OpenEXE"]) . "\" onclick=\"imgClick(this)\"><br>\n";
							echo "							" . htmlfix($AreaStore["WinSetup_N_OpenEXE"]);
							?>
						</li>
					</ol>
				</div>
				<img src="/img/articles/mbr2gpt/bootice-partman.png" class="flright">
				<p class="w3-small">
<?php ParaAreaStd($AreaStore["Bootice_PartMan"]); ?>
				</p>
				<p class="w3-small">
<?php ParaAreaStd($AreaStore["Bootice_Path"]); ?>
				</p>
				<?php
				echo "<img src=\"/img/articles/mbr2gpt/bootice_Win10.png\" class=\"w3-hover-opacity flright\" alt=\"" . htmlfix($AreaStore["PICT_B_W10"]) . "\" onclick=\"imgClick(this)\">\n";
				echo "				<p class=\"w3-small\">";
				ParaAreaStd($AreaStore["Bootice_AllBoot"]);
				echo "				</p>";
				echo "				";
				echo "<img src=\"/img/articles/mbr2gpt/bootice-RecoGUID.png\" class=\"w3-hover-opacity flright\" alt=\"" . htmlfix($AreaStore["PICT_BCD_GUID_R"]) . "\" onclick=\"imgClick(this)\">\n";
				echo "				<p class=\"w3-small\">";
				ParaAreaStd($AreaStore["Bootice_dropReco"]);
				echo "				</p>";
				?>
			</div>
			<!-- Partitioning -->
			<div class="w3-card-4 w3-padding" style="background-color: white; overflow: auto; width: 98%; margin-top: 5px;">
				<h2><?php echo $AreaStore["Partitioning"]; ?></h2>
				<p class="w3-small">
<?php ParaAreaStd($AreaStore["Linux_Start_GPARTED"]); ?>
				</p>
				<h3><?php echo $AreaStore["Gparted_DupReco"]; ?></h3>
				<?php
				echo "<img src=\"/img/articles/mbr2gpt/gparted_delDupReco.png\" class=\"w3-hover-opacity flright\" alt=\"" . htmlfix($AreaStore["PICT_delReco"]) . "\" onclick=\"imgClick(this)\">\n";
echo "				<p class=\"w3-small\">\n";
ParaAreaStd($AreaStore["Gparted_DelDupReco"]);
echo "				</p>";
?>
				<h3><?php echo $AreaStore["Gparted_PartPos"]; ?></h3>
				<p class="w3-small">
<?php ParaAreaStd($AreaStore["Gparted_PartMove"]); ?>
				</p>
				<?php
echo "				<ol class=\"pict-lane pict-lane-no-numbers\">\n";
echo "					<li>\n";
echo "						<img src=\"/img/articles/mbr2gpt/gparted_MenResize.png\" class=\"w3-hover-opacity\" alt=\"" . htmlfix($AreaStore["PICT_GP_SelResize"]) . "\" onclick=\"imgClick(this)\">\n";
echo "					</li>\n";
echo "					<li>\n";
echo "						<img src=\"/img/articles/mbr2gpt/gparted_RecoEnd1.png\" class=\"w3-hover-opacity\" alt=\"" . htmlfix($AreaStore["PICT_GP_Reco1"]) . "\" onclick=\"imgClick(this)\">\n";
echo "					</li>\n";
echo "					<li>\n";
echo "						<img src=\"/img/articles/mbr2gpt/gparted_WinMove.png\" class=\"w3-hover-opacity\" alt=\"" . htmlfix($AreaStore["PICT_WinMove"]) . "\" onclick=\"imgClick(this)\">\n";
echo "					</li>\n";
echo "					<li>\n";
echo "						<img src=\"/img/articles/mbr2gpt/gparted_MoveSysRes.png\" class=\"w3-hover-opacity\" alt=\"" . htmlfix($AreaStore["PICT_GP_MvSysRes"]) . "\" onclick=\"imgClick(this)\">\n";
echo "					</li>\n";
echo "					<li>\n";
echo "						<img src=\"/img/articles/mbr2gpt/gparted_PreApplay.png\" class=\"w3-hover-opacity\" alt=\"" . htmlfix($AreaStore["PICT_GP_PreApply"]) . "\" onclick=\"imgClick(this)\">\n";
echo "					</li>\n";
echo "				</ol>\n";
				?>
			</div>
			<!-- BCD fix and mbr2gpt -->
			<div class="w3-card-4 w3-padding" style="background-color: white; overflow: auto; width: 98%; margin-top: 5px;">
				<h2><?php echo $AreaStore["DoMBR2GPT"]; ?></h2>
				<p class="w3-small">
<?php ParaAreaStd($AreaStore["WinPartConv"]); ?>
				</p>
				<h3><?php echo $AreaStore["FixBCD"]; ?></h3>
				<?php
				echo "<img src=\"/img/articles/mbr2gpt/bootice_FixBoot.png\" class=\"w3-hover-opacity flright\" alt=\"" . htmlfix($AreaStore["PICT_Reco2"]) . "\" onclick=\"imgClick(this)\">\n";
				echo "				<p class=\"w3-small\">";
				ParaAreaStd($AreaStore["Bootice_FixDrives"]);
				echo "				</p>";
				?>
				<h3><?php echo $AreaStore["ExecMBR2GPT"]; ?></h3>
				<p class="w3-small para-code-share">
<?php
$para = $AreaStore["RunMBR2GPT"];
$entity = htmlfix($para);
$nlstr = nl2br($entity);
$c_str = str_replace(array("*", "#"), array("<samp style=\"background-color: black; color: white;\">", "</samp>"), $nlstr);
echo wordwrap($c_str, 240, "\n", false) . "\n";
?>
				</p>
				<div class="para-code-share" style="background-color: black; color: white;">
					<code>
<?php
$promt = "X:\sources>cd ..\Windows\System32

X:\Windows\System32>MBR2GPT.EXE /validate /disk:0
MBR2GPT: Attempting to validate disk 0
MBR2GPT: Retrieving layout of disk
MBR2GPT: Validating layout, disk sector size is: 512 bytes
MBR2GPT: Validation completed successfully

X:\Windows\System32>MBR2GPT.EXE /convert /disk:0

MBR2GPT will now attempt to convert disk 0.
If conversion is successful the disk can only be booted in GPT mode.
These changes cannot be undone!

MBR2GPT: Attempting to convert disk 0
MBR2GPT: Retrieving layout of disk
MBR2GPT: Validating layout, disk sector size is: 512 bytes
MBR2GPT: Backing up the system partition
MBR2GPT: Creating the EFI system partition
MBR2GPT: Installing the new boot files
MBR2GPT: Performing the layout conversion
MBR2GPT: Migrating default boot entry
MBR2GPT: Adding recovery boot entry
MBR2GPT: Fixing drive letter mapping
MBR2GPT: Conversion completed successfully
MBR2GPT: Update WinRE config file
MBR2GPT: Before the new system can boot properly you need to switch the firmware to boot to UEFI mode!

X:\Windows\System32>";
$ent_str = htmlfix($promt);
$nlstr = nl2br($ent_str);
echo $nlstr;
?>
					</code>
				</div>
			</div>
		</div>
		<!-- The Modal -->
		<div id="pictmodal" class="modal">
			<span class="close" onclick="spanClick()">&times;</span>
			<img class="modal-content" id="modimg">
			<div id="caption"></div>
		</div>
    </body>
</html>
<?php mysqli_close($DBcon); ?>