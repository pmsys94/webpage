		<div id="Menu">
			<?php echo "<a href=\"/main.php" . $link_lang . "\" id=\"pagelogo\">PM.sys</a>\n";?>
			<div class="w3-dropdown-hover" id="pmsys-langdiv">
				<button class="w3-button">Languages</button>
				<div class="w3-dropdown-content w3-bar-block w3-card-4">
<?php
$script=$_SERVER['SCRIPT_NAME'];
$slash_inc_pos=strrpos($script, "/") + 1;
$script=substr($script, $slash_inc_pos);
while ($langs = mysqli_fetch_array($langR)) {
echo "                     <a href=\"" . $script . "?lang=" . $langs["abbreviation"] . "\" class=\"w3-bar-item w3-button\">" . $langs["DisplayValue"] . "</a>\n";
}
?>
				</div>
			</div>
			<div id="cats">
				<ul>
<?php printMenu($page["PointsMenu"], $page["HighlightsItem"], $lang, $DBcon, $link_lang);?>
				</ul>
			</div>
			<a class="menue-button menue-button-beschr-close" href="#Menu-zu">&equiv;</a>
			<a class="menue-button menue-button-beschr-open" href="#Menu">&equiv;</a>
		</div>