<?php
function printMenu($points, $highlight, $lang, $conection, $llang){
	# the bread crumbs array in the menu tree
	$crumbs[] = $points;
	findRoot($points, $conection, $crumbs);
	#give tree root a name from db
	$rootQ = "SELECT MenuKey FROM Menu WHERE SuperiorKey = '0';";
	$rootR = mysqli_query($conection, $rootQ);
	$root = mysqli_fetch_array($rootR);
	# walk tree down and on each node print leaves or go down node
	PrintArray($conection, $lang, $llang, $root["MenuKey"], $highlight, $crumbs, 45);
}
function findRoot($points, $connection, &$crumbs){
	# fetch the pointed array
	$pntarrayQ = sprintf("SELECT * FROM Menu WHERE MenuKey = '%s';", $points);
	$pntarrayR = mysqli_query($connection, $pntarrayQ);
	$pntarray = mysqli_fetch_array($pntarrayR);
	if($pntarray["SuperiorKey"] != "0"){# recursive call if not 0
		$crumbs[] = $points;
		findRoot($pntarray["SuperiorKey"], $connection, $crumbs);
	}
}
function PrintArray($connection, $lang, $llang, $menu, $highlight, &$crumbs, $subcolor){
	# fetch the Items
	$itemQ = sprintf("SELECT * FROM MenuItem mi join translates t on t.MenuItem=mi.ID WHERE mi.ConnectedMenu = '%s' and (t.lang = '%s' or t.lang = 'all') ORDER BY Rank;", $menu, $lang);
	$itemR = mysqli_query($connection, $itemQ);
	while ($items = mysqli_fetch_array($itemR)) {
		echo "					<li><a href=\"" . $items["URL"] . $llang . "\" style=\"background-color: hsl(0, 0%, " . $subcolor . "%);\">" . $items["EntryText"] . "</a></li>\n";
		if ($items["Submenu"] != "0") {# there is a mark for a submenu set
			if (IsBreadCrumb($items["Submenu"], $crumbs)) { # is it a submenu from the way upwards?
				PrintArray($connection, $lang, $llang, $items["Submenu"], $highlight, $crumbs, $subcolor+10); # before continue on the other leaves, go down to the pointed node
			}
		}
	}
}
function IsBreadCrumb($submen, &$crumbs){
	for ($i=0; $i < count($crumbs); $i++) { 
		if ($submen == $crumbs[$i]) {
			return TRUE;
		}
	}
	return FALSE;
}
?>