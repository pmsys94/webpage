<?php

function pullAreas($myCon, $lg, $page){
	$areas = [];
	$areaQ = sprintf("SELECT Area, TXT_Body FROM displaylang WHERE lang = '%s' AND PageID = %d;", $lg, $page);
	$areaR = mysqli_query($myCon, $areaQ);
	while($row = mysqli_fetch_array($areaR)){
		$areas[$row["Area"]] = $row["TXT_Body"];
	}
	return $areas;
}

function pullArea($myCon, $name, $lg, $page){
	$areaQ = sprintf("SELECT TXT_Body FROM displaylang WHERE Area = '%s' AND lang = '%s' AND PageID = %d;", $name, $lg, $page);
	$areaR = mysqli_query($myCon, $areaQ);
	$area = mysqli_fetch_array($areaR);
	return $area["TXT_Body"];
}

function htmlfix($area_txt){
	return htmlentities($area_txt, ENT_COMPAT | ENT_SUBSTITUTE);
}

function ParaAreaStd($area_txt){
	$entity = htmlfix($area_txt);
	$brStr = nl2br($entity);
	echo wordwrap($brStr, 240, "\n", false) . "\n";
}
?>