<?php
if(isset($_GET["lang"])){
	$lang = $_GET["lang"];
	$link_lang = "?lang=" . $_GET["lang"];
} else {
	$langAcc = explode(",", $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
	$lang = substr($langAcc[0], 0, 2);
	$link_lang = "";
}
include __DIR__ . "/dbcondat.inc";
include __DIR__ . "/menus.inc";
include __DIR__ . "/pullArea.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
# test lang is supported
$testQ = sprintf("SELECT COUNT(*) AS cnt FROM speaks WHERE lang = '%s' and Page = %d;", $lang, $pid);
$testR = mysqli_query($DBcon, $testQ);
if($testR){
	$test = mysqli_fetch_array($testR);
	if(($test["cnt"]) != 1){
		$lang = "de";
	}
} else { $lang = "de";}
$q = sprintf("SELECT * FROM Page join speaks on Page.ID=speaks.Page WHERE Page.ID = %d and speaks.lang = '%s';", $pid, $lang);
$r = mysqli_query($DBcon, $q);
$page = mysqli_fetch_array($r);
$langQ = sprintf("SELECT Language.* FROM Language join speaks on abbreviation=lang WHERE Page=%d AND NOT abbreviation='all';", $pid);
$langR = mysqli_query($DBcon, $langQ);
$AreaStore = pullAreas($DBcon, $lang, $pid);
?>