<!DOCTYPE HTML>
<html>
  <head>
 <?php
 $pid = 1;
include 'php/std-head.inc';
$AreaStore += pullAreas($DBcon, $lang, 0); // Also include dummy page areas
?>
        <link rel="icon" href="pm_fav.ico">
        <title><?php echo $page["HTML_Title"]; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/styles/w3.css">
        <link rel="stylesheet" href="./styles/menu_and_main.css" type="text/css">
        <style type="text/css">
          hr {
            border-top: 1px solid black;
          }
          img.microTopic{
            width: 100%;
            height: auto;
            pointer-events: none;
          }
          #rquarter-top{
            margin-top: unset;
          }
          .mainTop{
            width: 100%;
          }
          .mainGroupDiv{
            float: left;
          }
          .mainGroupDiv h5{
            margin-left: 43%;
          }
          div#ldiv{
            margin-right: 0px;
          }
          div#rdiv{
            margin-left: 0px;
          }
          @media screen and (min-width: 601px){
            h4#mainSide{
              width: 210px;
              margin-left: 43%;
            }
            #rquarter-top{
              margin-top: 45%;
            }
            .mainTop{
              width: 40%
            }
            div#ldiv{
              margin-right: 5px;
              margin-left: 10%;
            }
            div#rdiv{
              margin-left: 5px;
            }
            #img-pi{
              width: 75%;
            }
          }
        </style>
 </head>
 <body>
<?php include 'php/std-menu-head.inc'; echo "\n";?>
    <div id="Inhalt">
      <div class="w3-threequarter w3-padding-large">
        <h3>PM.sys - Software, Raspberry Pi &amp; Arduino</h3>
        <p>
          <?php ParaAreaStd($AreaStore["headPara1"]); ?>
          <?php echo "<a href=\"aboutMe/CV.php" . $link_lang . "\">"; $str_here = $AreaStore["linkHere"]; echo $str_here; ?></a>
          <?php ParaAreaStd($AreaStore["headPara2"]); ?>
        </p>
      <h4 id="mainSide"><?php echo $AreaStore["h4Side"]; ?></h4>
      
        <div id="ldiv" class="w3-card mainTop mainGroupDiv">
          <h5><?php echo "<a href=\"/hw/" . $link_lang . "\">"; ?>Hardware</a></h5>
          <h6><?php echo "<a href=\"hw/pveCloud/" . $link_lang . "\">" . $AreaStore["pveCloud-Main"] . "</a>"; ?></h6>
          <p>
            <?php ParaAreaStd($AreaStore["pveCloud-Topic"]); ?>
          </p>
          <img class="microTopic" src="https://www.proxmox.com/images/proxmox/screenshots/Proxmox-VE-7-4-darktheme.png">
          <p class="w3-tiny">
            Screenshot PVE original published by <a href="https://www.proxmox.com/en/news/media-kit">Proxmox Server Solutions GmbH</a>.
          </p>
          <hr>
          <h6><?php echo "<a href=\"hw/rpi/" . $link_lang . "\">"; ?>Raspberry Pi</a></h6>
          <p>
            <?php echo $AreaStore["paraRPI"] . "\n"; ?>
          </p>
          <a href="https://www.raspberrypi.org/">
            <img id="img-pi" class="microTopic" src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Raspberry-Pi-Zero-FL.jpg" style="pointer-events: unset;" alt="Photo Raspberry Zero by Evan Amos (link below)">
          </a>
          <p class="w3-tiny">
            Photo by <a href="https://commons.wikimedia.org/wiki/User:Evan-Amos">Evan Amos</a> published on <a href="https://de.wikipedia.org/wiki/Raspberry_Pi">Wikipedia Article about Raspberry Pi</a>.
          </p>
          <hr>
          <h6><?php echo "<a href=\"hw/ino/" . $link_lang . "\">"; ?>Arduino</a></h6>
          <p>
            <?php echo $AreaStore["paraINO"] . "\n"; ?>
          </p>
          <img class="microTopic" style="width: 180px;" src="https://upload.wikimedia.org/wikipedia/commons/8/87/Arduino_Logo.svg">
        </div>
        <div id="rdiv" class="w3-card mainTop mainGroupDiv">
          <h5><?php echo "<a href=\"/sw/" . $link_lang . "\">"; ?>Software</a></h5>
          <h6><?php echo "<a href=\"sw/webbird.php" . $link_lang . "\">"; ?>Webbird Browser (Windows VB.NET)</a></h6>
          <p>
            <?php ParaAreaStd($AreaStore["paraWB"]); ?>        
          </p>
          <img class="microTopic" src="/img/WB-Screenshot.png">
          <hr>
          <h6><?php echo "<a href=\"sw/mediadb.php" . $link_lang . "\">". $AreaStore["header_MDB"]; ?></a></h6>
          <p>
            <?php ParaAreaStd($AreaStore["paraMediaDB"]); ?>   
          </p>
          <img class="microTopic" src="/img/mediadb/screen-web-desktop.png">
        </div>
      </div>
      <div class="w3-quarter w3-padding">
      	<div id="rquarter-top" class="w3-card-4">
          <h5><?php echo $AreaStore["headAthis"]; ?></h5>
          <p>
             <?php ParaAreaStd($AreaStore["athis"]); echo "<a href=\"aboutThis.php" . $link_lang . "\">";echo $str_here; ?>...</a>
          </p>
        </div>
        <div class="w3-card-4">
          <h5><?php echo $AreaStore["headRHG"]; ?></h5>
          <p>
             <?php ParaAreaStd($AreaStore["paraRHG"]); echo "<a href=\"hw/rhg.php" . $link_lang . "\">";echo $str_here; ?>...</a>
          </p>
          <img class="microTopic" src="/img/rhg600.jpg">
        </div>
        
      </div>
    </div>
 </body>
</html>
<?php
mysqli_close($DBcon);
?>