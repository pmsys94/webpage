// Get the modal
var modal = null;

// Get the image and insert it inside the modal
var modalImg1 = null;
var modalImg2 = null;
var modalImg3 = null;
var para = null;

function zv3_i1c(img, id, showImages){// req. every picture in onclick event
	var smallScreen = true;
	if(document.body.clientWidth >= 700){smallScreen = false;}
  modal.style.display = "block";
  modalImg1.src = img.src;
  bunker=document.getElementById(id);
  if(showImages > 1){
  	modalImg2.style.display = "block";
    modalImg2.src = bunker.getElementsByTagName('IMG')[0].src;
    if(showImages > 2){
      modalImg3.style.display = "block";
      modalImg3.src = bunker.getElementsByTagName('IMG')[1].src;
    }
  }
  if(smallScreen){
  	modalImg1.style.width = "100%";
  	modalImg3.style.width = "100%";
  	modalImg2.style.width = "100%";
  } else {
  	if(showImages == 3){
	    modalImg3.style.width = "26.66%";
	    modalImg2.style.width = "26.66%";
	    modalImg1.style.width = "26.66%";

	    modalImg3.style.marginLeft = "5%";
	    modalImg2.style.marginLeft = "5%";
	    modalImg1.style.marginLeft = "5%";
  	} else if(showImages == 2){
	    modalImg2.style.width = "35%";
	    modalImg1.style.width = "35%";

	    modalImg2.style.marginLeft = "10%";
	    modalImg1.style.marginLeft = "10%";
  	} else {
  		modalImg1.style.float = "initial";
  	}
  }
  para.innerHTML = bunker.getElementsByTagName('P')[0].innerHTML;
}

// When the user clicks on <span> (x), close the modal
function zv3_sCc() {
  modal.style.display = "none";
  modalImg1.style.width = "80%";
  modalImg1.style.marginLeft = "auto";
  modalImg2.style.marginLeft = "unset";
  modalImg2.style.display = "none";
  modalImg3.style.display = "none";
  modalImg1.style.float = "";
}// req. span tag with onclick event

function zview3_init(){
	modal = document.getElementById("zview3");
	modalImg1 = document.getElementById("modimg1");
	modalImg2 = document.getElementById("modimg2");
	modalImg3 = document.getElementById("modimg3");
	para = document.getElementById("modpara");
}