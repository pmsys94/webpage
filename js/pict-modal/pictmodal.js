// Get the modal
var modal = null;

// Get the image and insert it inside the modal - use its "alt" text as a caption
var modalImg = null;
var captionText = null;

function imgClick(img){// req. every picture in onclick event
  modal.style.display = "block";
  modalImg.src = img.src;
  captionText.innerHTML = img.alt;
}

// When the user clicks on <span> (x), close the modal
function spanClick() {
  modal.style.display = "none";
}// req. span tag with onclick event

function pictmodal_init(){
	modal = document.getElementById("pictmodal");
	modalImg = document.getElementById("modimg");
	captionText = document.getElementById("caption");
}